/*
 * Utilities.java
 * 
 * Created on Jul 20, 2007, 9:01:50 PM
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.bionicmessage.funambol.framework;

/**
 *
 * @author matt
 */
public class Utilities {

    /** Replace illegal characters in file path that could
     * turn up from sync source names etc.
     * Thanks to USA.net for contributing this
     */
    public static String cleanFilePath(String path) {
        path = path.replace("/","_");
        path = path.replace("\\", "_");
        path = path.replace(":", "_");
        path = path.replace("*", "_");
        path = path.replace("?", "_");
        path = path.replace("\"", "_");
        path = path.replace("<", "_");
        path = path.replace(">", "_");
        path = path.replace("|", "_");
        return path;
    }
}
