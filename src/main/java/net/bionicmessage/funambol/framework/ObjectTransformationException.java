/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.framework;
/**
 *  <ul><b><i>ObjectTransformationException</i></b>is thrown due to 
 *    transformation problems between one datatype and another 
 *      </ul>
 */
// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.47082F67-1D24-85B1-E106-0A5D7EAD345C]
// </editor-fold> 
public class ObjectTransformationException extends java.lang.Exception {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.DA87A3CE-D7D2-76C2-F613-03A226848839]
    // </editor-fold> 
    public ObjectTransformationException (String message) {
        super(message);
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.7338BDBE-E6E5-0DFA-612D-8D92607F5981]
    // </editor-fold> 
    public ObjectTransformationException (String message, Throwable origin) {
        super(message, origin);
    }

}

