package net.bionicmessage.funambol.framework;
/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  <ul><i>InboundObject </i>represents a SourceObject that has been passed to 
 *    the connector from Funambol
 *      </ul>
 */
// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.FD75B790-3C2D-A3FE-2F33-DEC10AFB7EC4]
// </editor-fold> 
public interface InboundObject extends SourceObject {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.95BB2C68-EE7E-12DF-E343-F94646437F8E]
    // </editor-fold> 
    public void setDestinationStore (String destination);

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.C18B4531-88F9-9769-6D13-734EF4CFD61C]
    // </editor-fold> 
    public String getDestinationStore ();

}

