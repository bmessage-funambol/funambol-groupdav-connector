/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.framework;

import java.util.Properties;

/**
 *  <ul><b><i>SourceObject</i></b> represents a PDI object as it passes through 
 *        from
 *  one store, via the pipeline to another.
 *  
 *      </ul>
 */
// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.564035D4-BA82-D83C-A10D-9EA05CEB5390]
// </editor-fold> 
public interface SourceObject {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.E788F1B4-1338-0817-ABCE-B19B061E458C]
    // </editor-fold> 
    public Object getPDIDataObject ();

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.AA76C530-15A9-6591-B48F-B8F3A30E0DB1]
    // </editor-fold> 
    public Class getPDIDataClass ();

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.F5EABD11-02B2-8791-37AD-D2965D83D763]
    // </editor-fold> 
    public String getStringRepresentation (String type) throws ObjectTransformationException;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.28AED3DA-2178-9BFD-D229-EF93E2A5BA70]
    // </editor-fold> 
    public void parseIntoObjectFromString (String str, String type) throws ObjectTransformationException;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D52DA9EB-5A3D-C6D3-CC3E-D1E2F469F46D]
    // </editor-fold> 
    public String getDefaultStringOutputType ();

}

