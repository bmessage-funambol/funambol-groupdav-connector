/*
 * Constants.java
 * 
 * Created on Jul 20, 2007, 9:09:18 PM
 * 
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.bionicmessage.funambol.framework;

import net.bionicmessage.objects.StoreConstants;

/**
 * String constants for connector operation
 * @author matt
 */
public class Constants {
    /** connector.storepath */
    public static final String STOREDIR_PATH = "connector.storepath";
    /** connector.singlelog */
    public static final String SINGLE_LOG = "connector.singlelog";
    /** connector.calendarlog */
    public static final String CALENDAR_LOGGER_NAME = "connector.calendarlog";
    /** connector.contacts */
    public static final String CONTACT_LOGGER_NAME = "connector.contacts";
    /** connector.notelog */
    public static final String NOTE_LOGGER_NAME = "connector.calendarlog";
    /** connector.serverhost */
    public static final String SERVER_HOST = "connector.serverhost";
    /** connector.sources. */
    public static final String SOURCE_LOCATION_BASE = "connector.sources.";
    /**
     * @deprecated 
     * connector.todomode */
    public static final String SOURCE_IS_TASK = "connector.todomode";
    /**
     * connector.mixedmode
     */
    public static final String MIXED_MODE = "connector.mixedmode";
    /** connector.utcrelativeallday */
    public static final String UTC_RELATIVE_ALLDAY = "connector.utcrelativeallday";
    /** connector.convertalarms */
    public static final String CONVERT_ALARMS = "connector.convertalarms";
    /** */
    public static final String INBOUND_PROCESSOR = "";
    
    /** text/x-s4j-sife */
    public static final String TYPE_CALENDAR_SIF = "text/x-s4j-sife";
    /** text/x-vcalendar */
    public static final String TYPE_CALENDAR_VCAL = "text/x-vcalendar";
    /** text/x-sj-sift */
    public static final String TYPE_TASK_SIF = "text/x-s4j-sift";
    /** text/calendar */
    public static final String TYPE_CALENDAR_2445 = "text/calendar";

    /** text/x-vcard */
    public static final String TYPE_CONTACT_VCARD21 = "text/x-vcard";
    /** text/vcard */
    public static final String TYPE_CONTACT_VCARD30 = "text/vcard";
    /** text/x-s4j-sifc */
    public static final String TYPE_CONTACT_SIFC = "text/x-s4j-sifc";
    
    /** text/x-s4j-sifn */
    public static final String TYPE_NOTE_SIFN = "text/x-s4j-sifn";
    /** text/x-vnote */
    public static final String TYPE_NOTE_VNOTE = "text/x-vnote";
    /** text/calendar */
    public static final String TYPE_NOTE_ICAL = "text/calendar";
    /** text/plain */
    public static final String TYPE_NOTE_PLAIN = "text/plain";
    
    public static final String DOMAIN_REPLACE = "%DOMAIN%";
    
   /* Constants for CalDAV filtering, just link to JGroupDAV ones */
    public static final String CALDAV_COMPONENT = 
            StoreConstants.CALDAV_COMPARE_COMPONENT;
    public static final String CALDAV_PROPERTY =
            StoreConstants.CALDAV_COMPARE_PROPERTY;
    public static final String CALDAV_START = 
            StoreConstants.CALDAV_COMPARE_START;
    public static final String CALDAV_END =
            StoreConstants.CALDAV_COMPARE_END;

    /** Force CardDAV methods rather than auto detect */
    public static final String STORE_FORCE_CARDDAV = "store.forcecarddav";

    public static final String REPLACE_USER_ID = "connector.replaceuserid";
         
}
