/* GroupDAV connector for Funambol
 * Copyright (C) 2007-2010  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package net.bionicmessage.funambol.datakit;

import com.funambol.common.pim.utility.TimeUtils;
import com.funambol.framework.engine.source.SyncContext;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.component.VToDo;

/**
 * Converts Funambol calendar instances to ical4j
 * @author matt
 */
public class funambol2ical4j {

    public static final Logger log = Logger.getLogger("groupdav.fnblical");

    public static net.fortuna.ical4j.model.Calendar convertFunambolEvent2ical4jEvent(
            com.funambol.common.pim.calendar.Calendar fnblcal,
            Properties props,
            SyncContext ctx) throws ObjectTransformationException {

        /* Needed for dtstart/dtend all days as we get different
         * formats
         */
        String formattedDtStartDate = "";
        String formattedDtEndDate = "";

        /* Do the same thing here as we did in ical4j2funambol. Pick out each
         * property we want and process it one by one */
        net.fortuna.ical4j.model.Calendar ical4jcal =
                new net.fortuna.ical4j.model.Calendar();
        net.fortuna.ical4j.model.component.CalendarComponent cc = null;
        if (fnblcal.getCalendarContent() instanceof com.funambol.common.pim.calendar.Event) {
            cc = new net.fortuna.ical4j.model.component.VEvent();
        } else if (fnblcal.getCalendarContent() instanceof com.funambol.common.pim.calendar.Task) {
            cc = new net.fortuna.ical4j.model.component.VToDo();
        }

        ical4jcal.getComponents().add(cc);
        /* if (props != null && props.getProperty("connector.timezone") != null)
        tid = attachVTimeZone(
        props.getProperty("connector.timezone"),
        ical4jcal); */
        String tz = null;
        if (ctx.getPrincipal().getDevice().getConvertDatePolicy() == 0) {
            tz = ctx.getPrincipal().getDevice().getTimeZone();
        }
        net.fortuna.ical4j.model.property.DtStart dts = null;
        net.fortuna.ical4j.model.property.DtEnd dte = null;
        com.funambol.common.pim.calendar.CalendarContent calcontent = fnblcal.getCalendarContent();
        com.funambol.common.pim.common.Property uid = calcontent.getUid();
        net.fortuna.ical4j.model.property.ProdId pri = new net.fortuna.ical4j.model.property.ProdId("-//BionicMessage Funambol Connector//funambol2ical4jconvert//EN");
        ical4jcal.getProperties().add(pri);
        ical4jcal.getProperties().add(net.fortuna.ical4j.model.property.Version.VERSION_2_0);
        if (uid != null && uid.getPropertyValueAsString() != null) {
            net.fortuna.ical4j.model.property.Uid ical4juid = new net.fortuna.ical4j.model.property.Uid(uid.getPropertyValueAsString());
            cc.getProperties().add(ical4juid);
        }
        com.funambol.common.pim.common.Property dtstart = calcontent.getDtStart();
        com.funambol.common.pim.common.Property dtend = calcontent.getDtEnd();

        /* com.funambol.common.pim.common.Property clazz = fnblevent.getClassEvent();
        if (clazz.getPropertyValue() != null) {
        net.fortuna.ical4j.model.property.Clazz ical4jclazz =
        new net.fortuna.ical4j.model.property.Clazz(clazz.getPropertyValueAsString());
        ical4jevent.getProperties().add(ical4jclazz);
        } */

        if (hasValue(dtstart)) {
            try {
                String dtstartdate = dtstart.getPropertyValueAsString();
                if (dtstartdate.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_LENGTH) {
                    if (props.getProperty(Constants.UTC_RELATIVE_ALLDAY) == null) {
                        dts = new net.fortuna.ical4j.model.property.DtStart();
                        dts.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE);
                        formattedDtStartDate = dtstartdate.replace("-", "");
                        dts.setValue(formattedDtStartDate);
                    } else {
                        net.fortuna.ical4j.model.DateTime deds = createOGo1AllDayDtStart(dtstartdate,
                                ctx.getPrincipal().getDevice().getTimeZone());
                        dts = new net.fortuna.ical4j.model.property.DtStart(deds);
                    }
                } else {
                    net.fortuna.ical4j.model.DateTime dtdt = parseIntoDateTime(dtstartdate, tz);
                    dts = new net.fortuna.ical4j.model.property.DtStart(dtdt);

                }
                cc.getProperties().add(dts);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "Error thrown in dtstart conversion: {0}", ex.getMessage());
                throw new ObjectTransformationException("Error in DTSTART conversion", ex);
            }
        }

        if (hasValue(dtend)) {
            try {
                String dtenddate = dtend.getPropertyValueAsString();
                /* CHECK: Funambol rolls dtend back if dtend(value=date)
                &&dtstart(value==date). If Funambol has rolled them back, stop it here */
                if (dtenddate.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_LENGTH ||
                        dtenddate.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYYMMDD_LENGTH) {
                    formattedDtEndDate = dtenddate.replace("-", "");
                    if (formattedDtStartDate.equals(formattedDtEndDate)) {
                        String rolledUp = TimeUtils.rollOneDay(dtenddate, true);
                        dte = new net.fortuna.ical4j.model.property.DtEnd();
                        dte.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE);
                        dte.setValue(rolledUp);
                    } else if (props.getProperty(Constants.UTC_RELATIVE_ALLDAY) == null) {
                        dte = new net.fortuna.ical4j.model.property.DtEnd();
                        dte.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE);
                        String formatted = dtenddate.replace("-", "");
                        dte.setValue(formatted);
                    } else {
                        net.fortuna.ical4j.model.DateTime dedt = createOGo1AllDayDtEnd(dtenddate,
                                ctx.getPrincipal().getDevice().getTimeZone());
                        dte = new net.fortuna.ical4j.model.property.DtEnd(dedt);
                    }
                } else {
                    net.fortuna.ical4j.model.DateTime dedt = parseIntoDateTime(dtenddate, tz);
                    dte = new net.fortuna.ical4j.model.property.DtEnd(dedt);
                }
                cc.getProperties().add(dte);
            } catch (Exception ex) {
                log.log(Level.SEVERE,"Could not convert dtend, {0}",ex.getMessage());
                throw new ObjectTransformationException("Error caught in dtend conversion", ex);
            }
        }
        com.funambol.common.pim.common.Property summ = calcontent.getSummary();
        if (hasValue(summ)) {
            net.fortuna.ical4j.model.property.Summary ical4jsumm =
                    new net.fortuna.ical4j.model.property.Summary(summ.getPropertyValueAsString());
            cc.getProperties().add(ical4jsumm);
        } else {
            net.fortuna.ical4j.model.property.Summary blankSumm =
                    new net.fortuna.ical4j.model.property.Summary("No summary entered");
            cc.getProperties().add(blankSumm);
        }
        com.funambol.common.pim.common.Property description = calcontent.getDescription();
        if (hasValue(description)) {
            net.fortuna.ical4j.model.property.Description ical4jdescription =
                    new net.fortuna.ical4j.model.property.Description(description.getPropertyValueAsString());
            cc.getProperties().add(ical4jdescription);
        }
        com.funambol.common.pim.common.Property location = calcontent.getLocation();
        if (hasValue(location)) {
            net.fortuna.ical4j.model.property.Location ical4jlocation =
                    new net.fortuna.ical4j.model.property.Location(location.getPropertyValueAsString());
            cc.getProperties().add(ical4jlocation);
        }
        com.funambol.common.pim.calendar.RecurrencePattern rp = calcontent.getRecurrencePattern();

        /**
         * BEGIN RECURRENCE CONVERSION
         */
        if (rp != null) {
            net.fortuna.ical4j.model.property.RRule ical4jrrule =
                    new net.fortuna.ical4j.model.property.RRule();
            net.fortuna.ical4j.model.Recur ical4jrecur = ical4jrrule.getRecur();
            String type = rp.getTypeDesc();
            net.fortuna.ical4j.model.WeekDayList wdList =
                    ical4jrecur.getDayList();

            List<String> dayOfWeekList = rp.getDayOfWeek();
            // only add BYDAY=x if we recur more than once a week
            if (dayOfWeekList.size() > 1) {
                for (String d : rp.getDayOfWeek()) {
                    net.fortuna.ical4j.model.WeekDay wd =
                            new net.fortuna.ical4j.model.WeekDay(d);
                    wdList.add(wd);
                }
            }
            if (rp.isNoEndDate()) {
                ical4jrecur.setCount(rp.getOccurrences());
                ical4jrecur.setInterval(rp.getInterval());
            } else {
                try {
                    String endDate = rp.getEndDatePattern();
                    net.fortuna.ical4j.model.DateTime endD = parseIntoDateTime(endDate, tz);

                    ical4jrecur.setUntil(endD);
                } catch (ParseException ex) {
                    java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE,
                            ex.getMessage(),
                            ex);
                }
            }
            if (type.equals("D")) {
                /* Daily recurrence */
                ical4jrecur.setFrequency(net.fortuna.ical4j.model.Recur.DAILY);
            } else if (type.equals("W")) {
                ical4jrecur.setFrequency(net.fortuna.ical4j.model.Recur.WEEKLY);
            } else if (type.equals("MP")) {
                ical4jrecur.setFrequency(net.fortuna.ical4j.model.Recur.MONTHLY);
                List<String> daysOfWeek = rp.getDayOfWeek();
                String day = (String) rp.getDayOfWeek().get(0);
                Short moffset = new Short(rp.getInstance());
                net.fortuna.ical4j.model.WeekDay wd =
                        new net.fortuna.ical4j.model.WeekDay(day);
                ical4jrecur.getDayList().add(wd);
            } else if (type.equals("MD")) {
                ical4jrecur.setFrequency(net.fortuna.ical4j.model.Recur.MONTHLY);
                Short monthdaymask = new Short(rp.getDayOfMonth());
                Integer mdmask = new Integer(monthdaymask.intValue());
                ical4jrecur.getMonthDayList().add(mdmask);
            } else if (type.equals("YM")) {
                // Grumble grumble.. we don't know if its MONTH or MONTH_NTH
                ical4jrecur.setFrequency(net.fortuna.ical4j.model.Recur.YEARLY);
                Short monthofyear = new Short(rp.getMonthOfYear());
                if (rp.getTypeId() == rp.TYPE_YEAR_NTH) {
                    Short moffset = new Short(rp.getInstance());
                    String day = (String) rp.getDayOfWeek().get(0);
                    net.fortuna.ical4j.model.WeekDay wd =
                            new net.fortuna.ical4j.model.WeekDay(day);
			// TODO: what above  moffset.intValue() ? don't think above 
			// is the correct place
                    ical4jrecur.getDayList().add(wd);
                    ical4jrecur.getMonthList().add(new Integer(monthofyear.intValue()));
                } else {
                    Short dayofmonth = new Short(rp.getDayOfMonth());
                    ical4jrecur.getMonthDayList().add(new Integer(dayofmonth.intValue()));
                    ical4jrecur.getMonthList().add(new Integer(monthofyear.intValue()));
                }
            }
            if (rp.getExceptions() != null &&
                    rp.getExceptions().size() > 0) {
                List<com.funambol.common.pim.calendar.ExceptionToRecurrenceRule> etrr =
                        rp.getExceptions();
                StringBuffer dateString = new StringBuffer();
                net.fortuna.ical4j.model.property.ExDate exDate =
                        new net.fortuna.ical4j.model.property.ExDate();
                boolean is_date = false;
                for (com.funambol.common.pim.calendar.ExceptionToRecurrenceRule et : etrr) {
                    if (dateString.length() > 0) {
                        dateString.append(",");
                    }
                    String date = et.getDate();
                    // Guaranteed to be in the wrong format, this slab is ripped from ExceptionToRecurrenceRule
                    String format = TimeUtils.getDateFormat(date.replaceAll("[Z:\\-]", ""));
                    if (format != null && TimeUtils.PATTERN_YYYY_MM_DD.equals(format)) {
                        try {
                            date = TimeUtils.convertDateFromTo(date, TimeUtils.PATTERN_YYYYMMDD);
                            is_date = true;
                        } catch (Exception ex) {
                            ex.printStackTrace(); // lets hope this enver happens
                        }
                    }
                    dateString.append(date);
                }
                if (is_date) {
                    exDate.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE);
                } else {
                    exDate.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE_TIME);
                }
                try {
                    exDate.setValue(dateString.toString());
                    cc.getProperties().add(exDate);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                
            }
            cc.getProperties().add(ical4jrrule);

            if (ctx.getPrincipal().getDevice().getTimeZone() != null) {
                TimeZoneRegistryFactory tzrf = TimeZoneRegistryFactory.getInstance();
                TimeZoneRegistry trr = tzrf.createRegistry();
                net.fortuna.ical4j.model.TimeZone devTz =
                        trr.getTimeZone(ctx.getPrincipal().getDevice().getTimeZone());
                VTimeZone vtz = devTz.getVTimeZone();
                ical4jcal.getComponents().add(vtz);
            }
        }

        /**
         * END RECURRENCE CONVERSION
         */
        com.funambol.common.pim.common.Property categories = calcontent.getCategories();
        if (hasValue(categories)) {
            net.fortuna.ical4j.model.property.Categories cat =
                    new net.fortuna.ical4j.model.property.Categories();
            cat.setValue(categories.getPropertyValueAsString());
            cc.getProperties().add(cat);
        }
        if (cc.getName().equals(VToDo.VTODO)) {
            com.funambol.common.pim.calendar.Task fnblTask = fnblcal.getTask();
            com.funambol.common.pim.common.Property priority = fnblTask.getPriority();
            if (hasValue(priority)) {
                net.fortuna.ical4j.model.property.Priority prip = new net.fortuna.ical4j.model.property.Priority();
                prip.setValue(priority.getPropertyValueAsString());
                cc.getProperties().add(prip);
            }
            /* com.funambol.common.pim.common.Property complete = fnblTask.getComplete();
            if (hasValue(complete)) {
            String value = complete.getPropertyValueAsString();
            if (value.equals("1")) {
            net.fortuna.ical4j.model.property.Status stat = new net.fortuna.ical4j.model.property.Status();
            stat.setValue("COMPLETED");
            cc.getProperties().add(stat);
            }
            } */
            com.funambol.common.pim.common.Property status = fnblTask.getComplete();
            if (hasValue(status)) {
                net.fortuna.ical4j.model.property.Status stat =
                        new net.fortuna.ical4j.model.property.Status();
                //todo: probably better if we use the defined mappings in S4j2IcalMapping
                Boolean statusValue = (Boolean) status.getPropertyValue();
                if (statusValue.booleanValue()) {
                    stat.setValue("COMPLETED");
                } else {
                    stat.setValue("IN-PROCESS");
                }

                cc.getProperties().add(stat);
            }

            // If we have DTEND instead of DUE, swap
            if (dte != null) {
                cc.getProperties().remove(dte);
                net.fortuna.ical4j.model.property.Due due =
                        new net.fortuna.ical4j.model.property.Due(dte.getDate());
                cc.getProperties().add(due);
            }

        }
        if (props.getProperty(Constants.CONVERT_ALARMS) != null) {
            com.funambol.common.pim.common.Property dalarm = calcontent.getDAlarm();
            if (hasValue(dalarm)) {
                try {
                    String[] attributes = dalarm.getPropertyValueAsString().split(";");
                    String dt = attributes[0];
                    if (!dt.contains("Z")) {
                        dt = dt.concat("Z");
                    }
                    attachVAlarm(dt, 0, cc, tz);
                } catch (ParseException ex) {
                    Logger.getLogger(funambol2ical4j.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (calcontent instanceof com.funambol.common.pim.calendar.Event) {
            com.funambol.common.pim.calendar.Event calEvent =
                    (com.funambol.common.pim.calendar.Event) calcontent;
            List<com.funambol.common.pim.calendar.Attendee> attendeeList = calEvent.getAttendees();
            Iterator<com.funambol.common.pim.calendar.Attendee> attItr = attendeeList.iterator();
            while (attItr.hasNext()) {
                com.funambol.common.pim.calendar.Attendee attendee = attItr.next();
                net.fortuna.ical4j.model.property.Attendee ical4jatt =
                        new net.fortuna.ical4j.model.property.Attendee();
                String cn = attendee.getName();
                net.fortuna.ical4j.model.parameter.Cn cnPara =
                        new net.fortuna.ical4j.model.parameter.Cn(cn);
                // Fix as INVDIVIDUAL for now.
                net.fortuna.ical4j.model.parameter.CuType cuPara =
                        net.fortuna.ical4j.model.parameter.CuType.INDIVIDUAL;
                ical4jatt.getParameters().add(cnPara);
                ical4jatt.getParameters().add(cuPara);
                String email = attendee.getEmail();
                if (email != null && email.length() > 0) {
                    email = "MAILTO:" + email;
                    try {
                        ical4jatt.setValue(email);
                        cc.getProperties().add(ical4jatt);
                    } catch (URISyntaxException ex) {
                        log.log(Level.SEVERE, "Error converting attendee email address", ex);
                    }
                }
                short status = attendee.getStatus();
                net.fortuna.ical4j.model.parameter.PartStat partStat =
                        AttendeeConverter.getPartStartFromShort(status);
                ical4jatt.getParameters().add(partStat);
            }
        }
        return ical4jcal;
    }

    public static net.fortuna.ical4j.model.DateTime parseIntoDateTime(String dt, String timezone) throws ParseException {
        SimpleDateFormat sdf = null;
        TimeZone tz = null;
        /* DANGER: Funambol will stick a stupid Z at the end of a datetime even when it isn't
         * UTC. Cut it off */
        if (dt.contains("Z") && dt.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_UTC_LENGTH) {
            sdf = new SimpleDateFormat(com.funambol.common.pim.utility.TimeUtils.PATTERN_UTC);
            if (timezone != null) {
                tz = TimeZone.getTimeZone(timezone);
            } else {
                tz = TimeZone.getTimeZone("UTC");
            }
            sdf.setTimeZone(tz);
        } else if (dt.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_UTC_WOZ_LENGTH) {
            sdf = new SimpleDateFormat(com.funambol.common.pim.utility.TimeUtils.PATTERN_UTC_WOZ);
        } /* else if (dt.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYYMMDD_LENGTH) {
            sdf = new SimpleDateFormat(com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYYMMDD);
        } */
        java.util.Date ds = sdf.parse(dt);
        net.fortuna.ical4j.model.DateTime dedt =
                new net.fortuna.ical4j.model.DateTime(ds);
        if (tz != null) {
            dedt.setUtc(true);
        }
        return dedt;
    }

    public static net.fortuna.ical4j.model.DateTime createOGo1AllDayDtStart(String dateString, String tz)
            throws ParseException, ObjectTransformationException {
        if (tz == null) {
            throw new ObjectTransformationException("Please set the timezone for the device for GMT All day mode");
        }
        SimpleDateFormat sdf = new SimpleDateFormat(com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD);
        TimeZone tzz = TimeZone.getTimeZone(tz);
        sdf.setTimeZone(tzz);
        java.util.Date ds = sdf.parse(dateString);
        net.fortuna.ical4j.model.DateTime deds = new net.fortuna.ical4j.model.DateTime(ds);
        deds.setUtc(true);
        return deds;

    }

    public static net.fortuna.ical4j.model.DateTime createOGo1AllDayDtEnd(String dateString, String tz)
            throws ParseException, ObjectTransformationException {
        if (tz == null) {
            throw new ObjectTransformationException("Please set the timezone for the device for GMT All day mode");
        }
        SimpleDateFormat sdf = new SimpleDateFormat(com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD);
        TimeZone tzz = TimeZone.getTimeZone(tz);
        sdf.setTimeZone(tzz);
        java.util.Date dt = sdf.parse(dateString);
        java.util.Calendar cl = java.util.Calendar.getInstance(tzz);
        cl.setTime(dt);
        cl.set(java.util.Calendar.HOUR, 23);
        cl.set(java.util.Calendar.MINUTE, 59);
        //cl.set(java.util.Calendar.SECOND, 59);
        net.fortuna.ical4j.model.DateTime dedt = new net.fortuna.ical4j.model.DateTime(cl.getTime());
        dedt.setUtc(true);
        return dedt;
    }

    public static String attachVTimeZone(String vtimezoneLoc, Calendar toAttach) {
        java.io.FileInputStream fis = null;
        try {
            java.io.File vloc = new java.io.File(vtimezoneLoc);

            fis = new java.io.FileInputStream(vloc);
            net.fortuna.ical4j.data.CalendarBuilder cbuild = new net.fortuna.ical4j.data.CalendarBuilder();
            net.fortuna.ical4j.model.Calendar vt = cbuild.build(fis);
            net.fortuna.ical4j.model.component.VTimeZone vs = (net.fortuna.ical4j.model.component.VTimeZone) vt.getComponents().get(0);
            net.fortuna.ical4j.model.property.TzId tzid = (net.fortuna.ical4j.model.property.TzId) vs.getProperty("TZID");

            toAttach.getComponents().add(vs);
            fis.close();
            return tzid.getValue();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE,
                    ex.getMessage(), ex);
        }
        return null;
    }

    public static void attachVAlarm(String atTime,
            int type,
            net.fortuna.ical4j.model.component.CalendarComponent toAttach,
            String tz) throws ParseException {
        net.fortuna.ical4j.model.component.VAlarm vl =
                new net.fortuna.ical4j.model.component.VAlarm();
        if (type == 0) { //display alarm

            vl.getProperties().add(net.fortuna.ical4j.model.property.Action.DISPLAY);
        }
        net.fortuna.ical4j.model.DateTime triggerTime = parseIntoDateTime(atTime, tz);
        /* net.fortuna.ical4j.model.property.Trigger trigger= new net.fortuna.ical4j.model.property.Trigger(triggerTime);
        vl.getProperties().add(trigger); */
        /* While its better for DateTime to go right into the event, which is legal,
         * clients don't always let us */
        // Find dtstart
        net.fortuna.ical4j.model.property.DtStart dts =
                (net.fortuna.ical4j.model.property.DtStart) toAttach.getProperty("DTSTART");
        net.fortuna.ical4j.model.Dur dur = new net.fortuna.ical4j.model.Dur(dts.getDate(), triggerTime);
        net.fortuna.ical4j.model.property.Trigger trigger =
                new net.fortuna.ical4j.model.property.Trigger(dur);
        vl.getProperties().add(trigger);
        net.fortuna.ical4j.model.property.Description alarmDescription =
                new net.fortuna.ical4j.model.property.Description("Event reminder");
        vl.getProperties().add(alarmDescription);
        if (toAttach instanceof net.fortuna.ical4j.model.component.VEvent) {
            net.fortuna.ical4j.model.component.VEvent ve =
                    (net.fortuna.ical4j.model.component.VEvent) toAttach;
            ve.getAlarms().add(vl);
        } else if (toAttach instanceof net.fortuna.ical4j.model.component.VToDo) {
            net.fortuna.ical4j.model.component.VToDo vt =
                    (net.fortuna.ical4j.model.component.VToDo) toAttach;
            vt.getAlarms().add(vl);
        }
    }

    private static boolean hasValue(com.funambol.common.pim.common.Property prop) {
        if (prop != null && prop.getPropertyValueAsString() != null &&
                prop.getPropertyValueAsString().length() > 0) {
            return true;
        }
        return false;
    }
}
