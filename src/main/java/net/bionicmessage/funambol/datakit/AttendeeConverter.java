/* GroupDAV connector for Funambol
 * Copyright (C) 2009 Mathew McBride <matt@mcbridematt.dhs.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package net.bionicmessage.funambol.datakit;

import java.net.URI;
import net.fortuna.ical4j.model.parameter.CuType;
import net.fortuna.ical4j.model.parameter.PartStat;

/**
 *
 * @author matt
 */
public class AttendeeConverter {

    public static com.funambol.common.pim.calendar.Attendee icalToFunambol(net.fortuna.ical4j.model.property.Attendee icalAttendee) {
        com.funambol.common.pim.calendar.Attendee funambolAttendee = null;
        URI attaddr = icalAttendee.getCalAddress();
        if (attaddr.toString().length() > 0) {
            funambolAttendee =
                new com.funambol.common.pim.calendar.Attendee();
            funambolAttendee.setUri(attaddr.toString());
            funambolAttendee.setName(icalAttendee.getParameter("CN").getValue());
            CuType ct = (CuType) icalAttendee.getParameter("CUTYPE");
            if (ct != null) {
                if (CuType.INDIVIDUAL.equals(ct)) {
                    funambolAttendee.setKind(com.funambol.common.pim.calendar.Attendee.INDIVIDUAL);
                } else if (CuType.GROUP.equals(ct)) {
                    funambolAttendee.setKind(com.funambol.common.pim.calendar.Attendee.GROUP);
                } else if (CuType.RESOURCE.equals(ct)) {
                    funambolAttendee.setKind(com.funambol.common.pim.calendar.Attendee.RESOURCE);
                } else if (CuType.ROOM.equals(ct)) {
                    funambolAttendee.setKind(com.funambol.common.pim.calendar.Attendee.ROOM);
                }
            }
            PartStat ps = (PartStat) icalAttendee.getParameter(PartStat.PARTSTAT);
            if (ps != null) {
                funambolAttendee = ical4j2funambol.attachPartStat(ps, funambolAttendee);
            }
            net.fortuna.ical4j.model.parameter.Role ro =
                    (net.fortuna.ical4j.model.parameter.Role) icalAttendee.getParameter("ROLE");
            if (ro != null) {
                if (net.fortuna.ical4j.model.parameter.Role.CHAIR.equals(ro)) {
                    funambolAttendee.setRole(com.funambol.common.pim.calendar.Attendee.CHAIRMAN);
                } else if (net.fortuna.ical4j.model.parameter.Role.OPT_PARTICIPANT.equals(ro)) {
                    funambolAttendee.setRole(com.funambol.common.pim.calendar.Attendee.OPTIONAL);
                } else if (net.fortuna.ical4j.model.parameter.Role.NON_PARTICIPANT.equals(ro)) {
                    funambolAttendee.setRole(com.funambol.common.pim.calendar.Attendee.NON_PARTICIPANT);
                } else if (net.fortuna.ical4j.model.parameter.Role.REQ_PARTICIPANT.equals(ro)) {
                    funambolAttendee.setRole(com.funambol.common.pim.calendar.Attendee.REQUIRED);
                }
            }
        }
        return funambolAttendee;
    }
    public static net.fortuna.ical4j.model.parameter.PartStat getPartStartFromShort(short status) {
        String partStatValue = null;
        switch (status) {
            case com.funambol.common.pim.calendar.Attendee.ACCEPTED:
                partStatValue = "ACCEPTED";
                break;
            case com.funambol.common.pim.calendar.Attendee.DECLINED:
                partStatValue = "DECLINED";
                break;
            case com.funambol.common.pim.calendar.Attendee.DELEGATED:
                partStatValue = "DELEGATED";
                break;
            default:
                partStatValue = "TENTATIVE";
                break;
        }
        net.fortuna.ical4j.model.parameter.PartStat ps = 
                new net.fortuna.ical4j.model.parameter.PartStat(partStatValue);
        return ps;
    }
}
