/*
 * ical4j2funambol.java
 *
 * Created on 26 November 2006, 15:44
 * Copyright (C) Mathew McBride 2006-2007
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package net.bionicmessage.funambol.datakit;

import java.text.ParseException;
import com.funambol.common.pim.calendar.RecurrencePattern;
import com.funambol.common.pim.calendar.RecurrencePatternException;
import com.funambol.common.pim.calendar.Task;
import com.funambol.common.pim.converter.CalendarStatus;
import com.funambol.common.pim.converter.VCalendarConverter;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.utility.TimeUtils;
import java.io.FileInputStream;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.Property.*;

import net.fortuna.ical4j.data.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.bionicmessage.utils.QPDecode;
import net.fortuna.ical4j.model.component.VAlarm;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VToDo;
import net.fortuna.ical4j.model.parameter.AltRep;
import net.fortuna.ical4j.model.parameter.Encoding;
import net.fortuna.ical4j.model.parameter.PartStat;
import net.fortuna.ical4j.model.parameter.Related;
import net.fortuna.ical4j.model.property.Attendee;
import net.fortuna.ical4j.model.property.Categories;
import net.fortuna.ical4j.model.property.DateProperty;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Due;
import net.fortuna.ical4j.model.property.ExDate;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.Priority;
import net.fortuna.ical4j.model.property.RRule;
import net.fortuna.ical4j.model.property.Status;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Transp;
import net.fortuna.ical4j.model.property.Trigger;
import net.fortuna.ical4j.model.property.Uid;

/**
 * Provides utilities for converting ical4j data objects to Fumambol
 * PDI objects
 * @author matt
 */
public class ical4j2funambol {

    /** Creates a new instance of ical4j2funambol */
    public static com.funambol.common.pim.calendar.Calendar convertIcal4jToFunambolEvent(net.fortuna.ical4j.model.Calendar ical4j_in, String clientType) throws RecurrencePatternException {
        long dstime = 0;
        long detime = 0;

        boolean isTodo = false;

        DtStart dts = null;
        String dtStartValueType = "";
        DtEnd dte = null;
        String dtEndValueType = "";
        
        /* QP decode a second time just to be sure. (the first is in object store/jgroupdav) */
        net.fortuna.ical4j.model.Calendar ical4j = QPDecode.decodeQP(ical4j_in);
        /* Setup a funambol event */
        com.funambol.common.pim.calendar.Calendar fnblcal = new com.funambol.common.pim.calendar.Calendar();
        com.funambol.common.pim.calendar.CalendarContent fnblcontent = null;
        Component comp = null;
        comp = (Component) ical4j.getComponents().getComponent(Component.VEVENT);
        if (comp == null) {
            comp = (Component) ical4j.getComponents().getComponent(Component.VTODO);
            fnblcontent = new com.funambol.common.pim.calendar.Task();
            fnblcal.setTask((Task) fnblcontent);
            isTodo = true;
        } else {
            fnblcontent = new com.funambol.common.pim.calendar.Event();
            fnblcal.setEvent((com.funambol.common.pim.calendar.Event) fnblcontent);
        }

        String startDate = ""; /* If we get DtStart, then cache it for Funambol recur */
        /* Blatantly obvious attributes */
        if (comp.getProperty(Uid.UID) != null) {
            Uid uid = (Uid) comp.getProperty(Uid.UID);
            com.funambol.common.pim.common.Property uidprop =
                    new com.funambol.common.pim.common.Property(uid.getValue());
            fnblcontent.setUid(uidprop);
        }
        /*   if (comp.getProperty(LastModified.LAST_MODIFIED) != null) {
        LastModified lm = (LastModified)comp.getProperty(LastModified.LAST_MODIFIED);
        com.funambol.common.pim.common.Property lmprop =
        new com.funambol.common.pim.common.Property();
        lm.setUtc(true);
        lmprop.setPropertyValue(lm.getValue());
        fnblcontent.setLastModified(lmprop);
        } */
        /* if (comp.getProperty(Clazz.CLASS) != null) {
        Clazz cl = (Clazz)comp.getProperty(Clazz.CLASS);
        com.funambol.common.pim.common.Property clprop =
        new com.funambol.common.pim.common.Property(cl.getValue());
        
        } */
        /* Start off with basic attributes.. DTSTART, DTEND, SUMMARY, DESCRIPTION, LOCATION,
        PRIORITY */
        // Do not convert DTSTART for VToDo (rejected by some clients)

        dts = (DtStart) comp.getProperty(DtStart.DTSTART);
        if (dts != null) {
            dts.setUtc(true);
            if (dts.getParameter(Parameter.VALUE) != null)
                dtStartValueType = dts.getParameter(Parameter.VALUE).getValue();
            dstime = dts.getDate().getTime();
        }
        dte = (DtEnd) comp.getProperty(DtEnd.DTEND);
        if (dte != null) {
            dte.setUtc(true);
            if (dte.getParameter(Parameter.VALUE) != null)
                dtEndValueType = dte.getParameter(Parameter.VALUE).getValue();
            detime = dte.getDate().getTime();
        }

        if (dtStartValueType.equals("DATE")
                && dtEndValueType.equals("DATE")) {
            /* This is an all day event */
            String dtsv = dts.getValue().concat("T000000");
            String dtev = dte.getValue();
            // Roll back dtend one day for Funambol
            dtev = TimeUtils.rollOneDay(dtev, false).concat("T235900");
            fnblcontent.getDtStart().setPropertyValue(dtsv);
            fnblcontent.getDtEnd().setPropertyValue(dtev);
            fnblcontent.setAllDay(true);
            startDate = dtsv;
        } else if (detime - dstime == 86340000 || detime - dstime == 86399000) {
            /** See if an all day event hasn't sneaked in via the back door */
            /* See the vObject minimum interoperability profile - OMA
             * (OMA-TS-vObjectOMAProfile-V1_0-20050118-C) for information on how to
             * correctly establish an all day event */
            fnblcontent.setAllDay(true);
            net.fortuna.ical4j.model.Date ds = new net.fortuna.ical4j.model.Date(dts.getDate().getTime());
            String dtsv = ds.toString().concat("T000000");
            net.fortuna.ical4j.model.Date dt = new net.fortuna.ical4j.model.Date(dte.getDate().getTime());
            String dtev = TimeUtils.rollOneDay(dt.toString(),false).concat("T235900");
            fnblcontent.getDtStart().setPropertyValue(dtsv);
            fnblcontent.getDtEnd().setPropertyValue(dtev);
            startDate=dtsv;
        } else {
            fnblcontent.setAllDay(false);
            if (dts != null) {
                fnblcontent.getDtStart().setPropertyValue(dts.getValue());
                startDate = dts.getValue();
            }
            if (dte != null) {
                fnblcontent.getDtEnd().setPropertyValue(dte.getValue());
            }
        }

       /*  if (comp.getProperty(DtStart.DTSTART) != null &&
                !comp.getName().equals(VToDo.VTODO)) {
            dts = (DtStart) comp.getProperty(DtStart.DTSTART);
            Parameter v = dts.getParameter("VALUE");
            if (v != null &&
                    v.getValue().equals("DATE")) {
                String newValue = dts.getValue();
                attachAllDayDtStart(fnblcontent, newValue, clientType);
            } else {
                dts.setUtc(true);
                com.funambol.common.pim.common.Property dtsprop = new com.funambol.common.pim.common.Property(dts.getValue());
                fnblcontent.setDtStart(dtsprop);
                dstime = dts.getDate().getTime();
                fnblcontent.setAllDay(false);
            }
            startDate = fnblcontent.getDtStart().getPropertyValueAsString();
        }
        if (comp.getProperty(DtEnd.DTEND) != null) {
            DtEnd dte = (DtEnd) comp.getProperty(DtEnd.DTEND);
            Parameter v = dte.getParameter("VALUE");
            if (v != null &&
                    v.getValue().equals("DATE")) {
                String newValue = dte.getValue();
                attachAllDayDtEnd(fnblcontent, newValue, clientType);
            } else {
                dte.setUtc(true);
                com.funambol.common.pim.common.Property dteprop =
                        new com.funambol.common.pim.common.Property(dte.getValue());
                fnblcontent.setDtEnd(dteprop);
                detime = dte.getDate().getTime();
                fnblcontent.setAllDay(false);
            }
        }
        if (comp.getProperty(Due.DUE) != null && comp.getProperty(DtEnd.DTEND) == null) {
            Due due = (Due) comp.getProperty(Due.DUE);
            Parameter v = due.getParameter("VALUE");
            if (v != null &&
                    v.getValue().equals("DATE")) {
                String newValue = due.getValue();
                attachAllDayDtEnd(fnblcontent, newValue, clientType);
            } else {
                due.setUtc(true);
                com.funambol.common.pim.common.Property dteprop =
                        new com.funambol.common.pim.common.Property(due.getValue());
                fnblcontent.setDtEnd(dteprop);
            }
        } */
        
        if (comp.getProperty(Summary.SUMMARY) != null) {
            /* We need to consider text encodings for this.
             * ical2 specifies 8 bit and base64 in spec, but vcal1
             * also allows quoted-printable (EVIL!!!). */
            Summary summ = (Summary) comp.getProperty(Summary.SUMMARY);
            com.funambol.common.pim.common.Property summprop =
                    new com.funambol.common.pim.common.Property();
            // Check for encodings
            if (summ.getParameter(Encoding.ENCODING) != null) {
                Encoding enc = (Encoding) summ.getParameter(Encoding.ENCODING);
                summprop.setEncoding(enc.getValue());
            }
            summprop.setPropertyValue(summ.getValue());
            fnblcontent.setSummary(summprop);
        }
        if (comp.getProperty(Description.DESCRIPTION) != null) {
            /* We also need to consider alternative text encoding here */
            Description des = (Description) comp.getProperty(Description.DESCRIPTION);
            com.funambol.common.pim.common.Property desprop =
                    new com.funambol.common.pim.common.Property();
            // Check for encdoings
            if (des.getParameter(Encoding.ENCODING) != null) {
                Encoding enc = (Encoding) des.getParameter(Encoding.ENCODING);
                desprop.setEncoding(enc.getValue());
            }
            desprop.setPropertyValue(des.getValue());
            fnblcontent.setDescription(desprop);
        }
        if (comp.getProperty(Location.LOCATION) != null) {
            Location loc = (Location) comp.getProperty(Location.LOCATION);
            com.funambol.common.pim.common.Property locprop =
                    new com.funambol.common.pim.common.Property();
            // Is our location pointing to a URL?
            if (loc.getParameter(AltRep.ALTREP) != null) {
                AltRep url = (AltRep) loc.getParameter(AltRep.ALTREP);
                locprop.setValue("URL");
                locprop.setType("VCARD");
            }
            locprop.setPropertyValue(loc.getValue());
            fnblcontent.setLocation(locprop);
        }
        if (comp.getProperty(Priority.PRIORITY) != null) {
            Priority pri = (Priority) comp.getProperty(Priority.PRIORITY);
            com.funambol.common.pim.common.Property priprop =
                    new com.funambol.common.pim.common.Property(pri.getValue());
            fnblcontent.setPriority(priprop);
        } else if (comp.getName().equals(VToDo.VTODO)) {
            com.funambol.common.pim.common.Property priprop =
                    new com.funambol.common.pim.common.Property("1");
            fnblcontent.setPriority(priprop);
        }
        /* Slightly harder */
        if (comp.getProperty(Transp.TRANSP) != null && fnblcontent instanceof com.funambol.common.pim.calendar.Event) {
            Transp tsp = (Transp) comp.getProperty(Transp.TRANSP);
            com.funambol.common.pim.common.Property tspprop =
                    new com.funambol.common.pim.common.Property();
            if (tsp.getValue().equals(Transp.OPAQUE)) {
                // This event WILL show up as busy
                tspprop.setPropertyValue("0");
            } else { // What calendar app couldn't chose between OPAQUE and TRANSPARENT?
                tspprop.setPropertyValue("1");
            }
            com.funambol.common.pim.calendar.Event e =
                    (com.funambol.common.pim.calendar.Event) fnblcontent;
            e.setTransp(tspprop);
        }
        if (comp.getProperty(Status.STATUS) != null &&
                fnblcontent instanceof com.funambol.common.pim.calendar.Task) {
            Status status = (Status) comp.getProperty(Status.STATUS);
            com.funambol.common.pim.common.Property stat =
                    new com.funambol.common.pim.common.Property();
            CalendarStatus cstatus = CalendarStatus.mapVcalIcalStatus(status.getValue());
            if (fnblcontent instanceof com.funambol.common.pim.calendar.Event) {
                stat.setPropertyValue(cstatus.getVCalICalValue(true));
            } else if (fnblcontent instanceof com.funambol.common.pim.calendar.Task) {
                stat.setPropertyValue(cstatus.getServerValue());
            }
            fnblcontent.setStatus(stat);
        }
        PropertyList attendees = comp.getProperties("ATTENDEE");
        List<com.funambol.common.pim.calendar.Attendee> fnblattendees =
                fnblcontent.getAttendees();
        Iterator attendeeIterator = attendees.iterator();
        while (attendeeIterator.hasNext()) {
            Attendee att = (Attendee) attendeeIterator.next();
            com.funambol.common.pim.calendar.Attendee fnblatt = AttendeeConverter.icalToFunambol(att);
            if (fnblatt != null) {
                fnblattendees.add(fnblatt);
            }
        }
        if (comp.getProperty(Categories.CATEGORIES) != null) {
            Categories cat = (Categories) comp.getProperty(Categories.CATEGORIES);
            fnblcontent.getCategories().setPropertyValue(cat.getValue());
        }
        if (comp.getProperty(RRule.RRULE) != null) {
            RRule rr = (RRule) comp.getProperty(RRule.RRULE);
            Recur r = rr.getRecur();
            ArrayList<com.funambol.common.pim.calendar.ExceptionToRecurrenceRule> 
                    exceptionList = new ArrayList(2);
            if (comp.getProperty(ExDate.EXDATE) != null) {
                ExDate ex = (ExDate)comp.getProperty("EXDATE");
                String value = ex.getValue();
                String[] params = value.split(",");
                for (int i = 0; i < params.length; i++) {
                    try {
                        String val = params[i];
                        com.funambol.common.pim.calendar.ExceptionToRecurrenceRule etrr = new com.funambol.common.pim.calendar.ExceptionToRecurrenceRule(false, val);
                        exceptionList.add(etrr);
                    } catch (ParseException ex1) {
                        ex1.printStackTrace();
                    }
                }
            }
            /* This is where things start getting insane. Funambol wants us to
             * find out every property of the recurrance rule and then use a custom
             * constructor for each case! */
            /* ical2 RULE VIOLATION: VEVENT may contain multiple RRULES... */
            String freq = r.getFrequency();
            
            int interval = (r.getInterval() > 0) ? r.getInterval() : 1;
            int count = r.getCount();
            boolean noEndDate = false;
            String endDate = "";
            if (r.getUntil() == null) {
                noEndDate = true;
            } else {
                net.fortuna.ical4j.model.Date eDate = 
                        new net.fortuna.ical4j.model.Date(r.getUntil()); 
                endDate = eDate.toString();
            }
            short dayOfWeekMask = 0;
            if (freq.equals(Recur.DAILY)) {
                try {
                    RecurrencePattern rp = RecurrencePattern.getDailyRecurrencePattern(interval,
                            startDate,
                            endDate,
                            noEndDate,
                            count,
                            dayOfWeekMask);
                    fnblcontent.setRecurrencePattern(rp);
                    
                } catch (RecurrencePatternException re) {
                    re.printStackTrace();
                // Send all RecurrencePatternExceptions to ground for now
                }
            } else if (freq.equals(Recur.HOURLY)) {
                /* NOT SUPPORTED BY VCAL1 AND FUNAMBOL */
            } else if (freq.equals(Recur.WEEKLY)) {
                /* Weekly */
                try {
                    ArrayList wkdaylist = r.getDayList();
                    for (int i = 0; i < wkdaylist.size(); i++) {
                        WeekDay wday = (WeekDay) wkdaylist.get(i);
                        String day = wday.getDay();
                        if (day.equals("SU")) {
                            dayOfWeekMask++;
                        } else if (day.equals("MO")) {
                            dayOfWeekMask = (short) (dayOfWeekMask + 2);
                        } else if (day.equals("TU")) {
                            dayOfWeekMask = (short) (dayOfWeekMask + 4);
                        } else if (day.equals("WE")) {
                            dayOfWeekMask = (short) (dayOfWeekMask + 8);
                        } else if (day.equals("TU")) {
                            dayOfWeekMask = (short) (dayOfWeekMask + 16);
                        } else if (day.equals("FR")) {
                            dayOfWeekMask = (short) (dayOfWeekMask + 32);
                        } else if (day.equals("SA")) {
                            dayOfWeekMask = (short) (dayOfWeekMask + 64);
                        }
                    }
                    RecurrencePattern rp = RecurrencePattern.getWeeklyRecurrencePattern(interval,
                            dayOfWeekMask,
                            startDate,
                            endDate,
                            noEndDate,
                            count);
                    fnblcontent.setRecurrencePattern(rp);
                } catch (RecurrencePatternException re) {
                    re.printStackTrace();
                }
            } else if (freq.equals(Recur.MONTHLY)) {
                /* Monthly - we have two sorts of month recurrance - by number day of month
                 * or day of week of month */
                // Do we have a set week position
                if (r.getDayList().size() > 0) {
                    // Get the weekday instance
                    WeekDay wd = (WeekDay) r.getDayList().get(0);
                    String day = wd.getDay();
                    int week = wd.getOffset();
                    Integer weekoffset = new Integer(week);
                    if (day.equals("SU")) {
                        dayOfWeekMask++;
                    } else if (day.equals("MO")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 2);
                    } else if (day.equals("TU")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 4);
                    } else if (day.equals("WE")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 8);
                    } else if (day.equals("TU")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 16);
                    } else if (day.equals("FR")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 32);
                    } else if (day.equals("SA")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 64);
                    }
                    RecurrencePattern rp = RecurrencePattern.getMonthNthRecurrencePattern(
                            r.getInterval(),
                            dayOfWeekMask,
                            weekoffset.shortValue(),
                            startDate,
                            endDate,
                            noEndDate,
                            r.getCount());
                    fnblcontent.setRecurrencePattern(rp);
                } else {
                    try {
                        java.util.Calendar mcal = java.util.Calendar.getInstance();
                        mcal.setTime(dts.getDate());
                        Integer dayOfMonth = new Integer(mcal.get(java.util.Calendar.DAY_OF_MONTH));
                        dayOfWeekMask = dayOfMonth.shortValue();
                        RecurrencePattern rp = RecurrencePattern.getMonthlyRecurrencePattern(interval,
                                dayOfWeekMask,
                                startDate,
                                endDate,
                                noEndDate,
                                count);
                        fnblcontent.setRecurrencePattern(rp);
                    } catch (RecurrencePatternException re) {
                        re.printStackTrace();
                        ;
                    }
                }
            } else if (freq.equals(Recur.YEARLY)) {
                // Same as monthly recurrence basically.
                if (r.getDayList().size() > 0) {
                    WeekDay wd = (WeekDay) r.getDayList().get(0);
                    String day = wd.getDay();
                    int week = wd.getOffset();
                    Integer weekoffset = new Integer(week);
                    Integer month = (Integer) r.getMonthList().get(0);
                    if (day.equals("SU")) {
                        dayOfWeekMask++;
                    } else if (day.equals("MO")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 2);
                    } else if (day.equals("TU")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 4);
                    } else if (day.equals("WE")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 8);
                    } else if (day.equals("TU")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 16);
                    } else if (day.equals("FR")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 32);
                    } else if (day.equals("SA")) {
                        dayOfWeekMask = (short) (dayOfWeekMask + 64);
                    }
                    RecurrencePattern rp = RecurrencePattern.getYearNthRecurrencePattern(r.getInterval(),
                            dayOfWeekMask,
                            month.shortValue(),
                            weekoffset.shortValue(),
                            startDate,
                            endDate,
                            noEndDate,
                            count);
                } else {
                    if (r.getInterval() < 0) {
                        r.setInterval(1);
                    }
                    try {
                        RecurrencePattern rp = RecurrencePattern.getYearlyRecurrencePattern(
                                r.getInterval(),
                                (short) 0,
                                (short) 0,
                                startDate,
                                endDate,
                                noEndDate,
                                count);
                        fnblcontent.setRecurrencePattern(rp);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } 
            if (fnblcontent.getRecurrencePattern() != null) { 
                try {
                    fnblcontent.getRecurrencePattern().setExceptions(exceptionList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        com.funambol.common.pim.common.Property prodid =
                new com.funambol.common.pim.common.Property("-//BionicMessage Funambol Connector//ical4jFunambolconvert//EN");
        fnblcal.setProdId(prodid);
        com.funambol.common.pim.common.Property ver =
                new com.funambol.common.pim.common.Property("1.0");
        fnblcal.setVersion(ver);
        return fnblcal;
    /* NOT YET CONVERTED: attach / attendee / exdate / exrule / geo / seq
     * recurid */
    }

    public static void attachAllDayDtEnd(com.funambol.common.pim.calendar.CalendarContent e, String newValue, String clientType) {
        if (clientType.equals("text/x-vcalendar")) {
            //newValue = newValue + "T240000";
            com.funambol.common.pim.common.Property dteprop =
                    new com.funambol.common.pim.common.Property(newValue);
            e.setAllDay(true);
            e.getDtEnd().setPropertyValue(newValue);
        } else if (clientType.equals("text/x-s4j-sife")) {
            e.getDtEnd().setPropertyValue(newValue);
            e.setAllDay(true);
        }
    }

    public static void attachAllDayDtStart(com.funambol.common.pim.calendar.CalendarContent e, String newValue, String clientType) {
        if (clientType.equals("text/x-vcalendar")) {
            //newValue = newValue + "T000000";
            com.funambol.common.pim.common.Property dtsprop =
                    new com.funambol.common.pim.common.Property(newValue);
            e.setAllDay(true);
            e.getDtStart().setPropertyValue(newValue);
        } else if (clientType.equals("text/x-s4j-sife")) {
            e.getDtStart().setPropertyValue(newValue);
            e.setAllDay(true);
        }
    }

    /** Process any alarms in the event/todo. Since this functionality is optional,
     * it is in another function
     */
    public static void attachAlarm(com.funambol.common.pim.calendar.CalendarContent e,
            net.fortuna.ical4j.model.component.CalendarComponent ical4je) {
        ComponentList alarms = null;
        if (ical4je instanceof VEvent) {
            VEvent ve = (VEvent) ical4je;
            alarms = ve.getAlarms();
        } else if (ical4je instanceof VToDo) {
            VToDo vt = (VToDo) ical4je;
            alarms = vt.getAlarms();
        } else {
            return;
        }
        Iterator alarmIterator = alarms.iterator();
        while (alarmIterator.hasNext()) {
            VAlarm va = (VAlarm) alarmIterator.next();

            Trigger t = va.getTrigger();
            String[] dalarmComponents = {"", "", "", ""};
            if (va.getAction().getValue().equals("DISPLAY")) {
                if (t.getDateTime() != null) {
                    dalarmComponents[0] = t.getDateTime().toString();
                } else if (t.getDuration() != null) {
                    Dur dur = t.getDuration();
                    DateProperty relatedTo = null;
                    if (t.getParameter(Related.RELATED) != null &&
                            t.getParameter(Related.RELATED).equals(Related.END)) {
                        relatedTo = (DateProperty) ical4je.getProperty(DtEnd.DTEND);
                    } else {
                        relatedTo = (DateProperty) ical4je.getProperty(DtStart.DTSTART);
                    }
                    Date dt = relatedTo.getDate();
                    // Calculate a new DateTime relative to dt
                    java.util.Calendar cal = java.util.Calendar.getInstance();
                    cal.setTime(dt);
                    cal.add(java.util.Calendar.SECOND, -(dur.getSeconds()));
                    cal.add(java.util.Calendar.MINUTE, -(dur.getMinutes()));
                    cal.add(java.util.Calendar.HOUR, -(dur.getHours()));
                    cal.add(java.util.Calendar.DAY_OF_YEAR, -(dur.getDays()));
                    cal.add(java.util.Calendar.WEEK_OF_YEAR, -(dur.getWeeks()));
                    DateTime newDate = new DateTime(cal.getTime());
                    newDate.setUtc(true);
                    dalarmComponents[0] = newDate.toString();
                }
            }
            StringBuffer buf = new StringBuffer();
            for (int i = 0; i < dalarmComponents.length; i++) {
                if (dalarmComponents[i] != null) {
                    buf.append(dalarmComponents[i]);
                }
                if (dalarmComponents.length != i - 1) {
                    buf.append(";");
                }
            }
            e.setDAlarm(new com.funambol.common.pim.common.Property(buf.toString()));
        }
    }
    public static com.funambol.common.pim.calendar.Attendee 
            attachPartStat(PartStat ps, 
            com.funambol.common.pim.calendar.Attendee att) {
        com.funambol.common.pim.calendar.Attendee fnblatt = att;
        if (PartStat.ACCEPTED.equals(ps)) {
            fnblatt.setStatus(com.funambol.common.pim.calendar.Attendee.ACCEPTED);
        } else if (PartStat.DECLINED.equals(ps)) {
            fnblatt.setStatus(com.funambol.common.pim.calendar.Attendee.DECLINED);
        } else if (PartStat.DELEGATED.equals(ps)) {
            fnblatt.setStatus(com.funambol.common.pim.calendar.Attendee.DELEGATED);
        } else if (PartStat.TENTATIVE.equals(ps)) {
            fnblatt.setStatus(com.funambol.common.pim.calendar.Attendee.TENTATIVE);
        } 
        return fnblatt;
    }
    /** Standalone tester */
    public static void main(String args[]) {
        if (args.length < 1) {
            System.err.println("Arguments: ical4j2funambol <file.ics>");
            return;
        }
        try {
            String file = args[0];
            FileInputStream fis = new FileInputStream(file);
            CalendarBuilder cbuild = new CalendarBuilder();
            Calendar ics = cbuild.build(fis);
            com.funambol.common.pim.calendar.Calendar fnblcal =
                    ical4j2funambol.convertIcal4jToFunambolEvent(ics, "text/x-vcalendar");
            VCalendarConverter vcc = new VCalendarConverter(null, "UTF-8",false);
            VCalendar vc = vcc.calendar2vcalendar(fnblcal, true);
            System.out.println(vc.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
