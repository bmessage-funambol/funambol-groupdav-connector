/*
 * ContactSyncSource.java
 *
 * Created on Oct 28, 2007, 12:13:41 PM
 *
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007-2008  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.contacts;

//import com.funambol.foundation.pdi.contact.Contact;
import com.funambol.common.pim.contact.Contact;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.SyncItemState;
import com.funambol.framework.engine.source.AbstractSyncSource;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSourceException;
import com.funambol.framework.tools.beans.BeanInitializationException;
import com.funambol.framework.tools.beans.LazyInitBean;
import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import net.bionicmessage.funambol.UserIdMappingUtil;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.Utilities;
import net.bionicmessage.objects.MultipleSourceVCardObjectStore;
import net.bionicmessage.objects.StoreConstants;
import net.bionicmessage.utils.HTMLFormatter;

/**
 *
 * @author matt
 */
public class ContactSyncSource extends AbstractSyncSource implements LazyInitBean, Serializable {

    private Properties connectorProperties = null;
    private MultipleSourceVCardObjectStore so = null;
    private FileHandler fh = null;
    private Logger log = null;
    private int syncType = 0;
    private Map itemStates = null;
    private Map sources = null;
    private Properties addrStoreProperties = null;
    private SyncContext ctx;

    private String mappedUserId = null;

    public ContactSyncSource() {
    }

    @Override
    public void beginSync(SyncContext syncContext) throws SyncSourceException {
        log = Logger.getLogger(Constants.CONTACT_LOGGER_NAME+"-"+System.currentTimeMillis());
        log.info("Begin sync for Contact Sync Source");
        this.ctx = syncContext;
        String principalName = Utilities.cleanFilePath(ctx.getPrincipal().getName());
        String storeDir = connectorProperties.getProperty(Constants.STOREDIR_PATH);
        File stdir = new File(storeDir + File.separatorChar + principalName + File.separatorChar);
        int storeopts = 0;
        try {
            /** Set up logging */
            if (!stdir.exists()) {
                stdir.mkdirs();
            }
            // Setup the logger
            HTMLFormatter hf = new HTMLFormatter();
            if (connectorProperties.getProperty(Constants.SINGLE_LOG) != null) {
                fh = new FileHandler(stdir.toString() + File.separatorChar + "connector.html");
                storeopts += MultipleSourceVCardObjectStore.OPTION_SINGLELOG;
            } else {
                long curTime = new java.util.Date().getTime();
                fh = new FileHandler(stdir.toString() + File.separatorChar + "connector-" + curTime + ".html");
            }
            fh.setFormatter(hf);
            log.addHandler(fh);
            log.setLevel(Level.ALL);

            itemStates = new Hashtable();
            syncType = ctx.getSyncMode();
            log.info("Beginning sync for " + principalName + " for mode=" + syncType);
            if (syncType == 201 || syncType == 205) {
                storeopts += 64;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.throwing("beginSync", "ContactSyncSource", e);
            throw new SyncSourceException(e);
        }
        // Perform setup
        itemStates = new Hashtable();
        syncType = ctx.getSyncMode();
        String creds = ctx.getPrincipal().getEncodedCredentials();
        sources = new Hashtable();


        addrStoreProperties = new Properties();

        if ("true".equals(connectorProperties.get(Constants.REPLACE_USER_ID))) {
            try {
                mappedUserId = UserIdMappingUtil.getIdForUser(syncContext.getPrincipal().getUsername());
            } catch (SQLException ex) {
                log.throwing(this.getClass().getName(), "beginSync", ex);
                throw new SyncSourceException("[User ID Mapping] cannot establish database connection",ex);
            } catch (NamingException ex) {
                log.throwing(this.getClass().getName(), "beginSync", ex);
                throw new SyncSourceException("[User ID Mapping] could not obtain JNDI service",ex);
            }
        }
        
        // Process the sources
        String defaultSourceLocation = null;
        
        Iterator configKeys = connectorProperties.keySet().iterator();
        while (configKeys.hasNext()) {
            String keyName = (String) configKeys.next();
            if (keyName.contains(Constants.SOURCE_LOCATION_BASE)) {
                String sourceLocation = connectorProperties.getProperty(keyName);
                sourceLocation = sourceLocation.replace("%USER%", syncContext.getPrincipal().getUsername());
                if (mappedUserId != null) {
                    sourceLocation = sourceLocation.replace("%USERID%",mappedUserId);
                }
                String sourceName = StoreConstants.BASE_PROPERTY_SOURCE.concat(keyName.replace(Constants.SOURCE_LOCATION_BASE, ""));
                if ("default".equals(sourceName))
                    defaultSourceLocation = sourceLocation;
                
                addrStoreProperties.setProperty(sourceName, sourceLocation);
            }
        }
        String domainString = connectorProperties.getProperty(Constants.SERVER_HOST);
        
        // Parse users email address
        if (domainString.contains(Constants.DOMAIN_REPLACE)) {
            String emailAddr = ctx.getPrincipal().getUser().getUsername();
            if (!emailAddr.contains("@")) {
                emailAddr = ctx.getPrincipal().getUser().getEmail();
            }
            String[] emailComponenets = emailAddr.split("@");
            String domain = emailComponenets[1];
            domainString = domainString.replace(Constants.DOMAIN_REPLACE, domain);
        }
        addrStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER,
                domainString);
        addrStoreProperties.setProperty(StoreConstants.PROPERTY_STORE_LOCATION, storeDir);
        String collectionMode = connectorProperties.getProperty(StoreConstants.PROPERTY_SERVER_MODE);
        if (collectionMode != null)
            addrStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER_MODE, collectionMode);

        String cardDAVForceProperty = connectorProperties.getProperty(Constants.STORE_FORCE_CARDDAV);
        
        // Create store
        so = new MultipleSourceVCardObjectStore(stdir.toString(), storeopts);
        so.setProperties(addrStoreProperties);
        so.loadSourcesFromProps();
        so.setAddrMode(true);
        try {
            URI serverURI = new URI(domainString+defaultSourceLocation);
            if ("true".equals(cardDAVForceProperty))
                so.setServerCardDAV(serverURI, creds);
            else
                so.setServer(serverURI, creds);
            // Sync
            so.startSync();
        } catch (Exception e) {
            e.printStackTrace();
            log.throwing("ContactSyncSource", e.getMessage(), e);
            throw new SyncSourceException(e);
        }
    }

    public SyncItemKey[] getAllSyncItemKeys() {
        log.info("getAllSyncItemKeys()");
        SyncItemKey[] newKeys = this.getNewSyncItemKeys(null, null);
        SyncItemKey[] updatedKeys = this.getUpdatedSyncItemKeys(null, null);
        SyncItemKey[] allKnownKeys = new SyncItemKey[newKeys.length + updatedKeys.length];
        int curKey = 0;
        for (int i = 0; i < newKeys.length; i++) {
            allKnownKeys[curKey] = newKeys[i];
            curKey++;
        }
        for (int i = 0; i < updatedKeys.length; i++) {
            allKnownKeys[curKey] = updatedKeys[i];
            curKey++;
        }
        return allKnownKeys;
    }

    /** Lists all deleted items since last sync.
     * 
     * @param sinceTs Not used
     * @param untilTs Not used
     * @throws com.funambol.framework.engine.source.SyncSourceException
     */
    public SyncItemKey[] getDeletedSyncItemKeys(java.sql.Timestamp sinceTs, java.sql.Timestamp untilTs)
            throws SyncSourceException {
        log.info("getDeletedSyncItemKeys()");
        ArrayList deletedUIDS = so.getDeletedFromStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[deletedUIDS.size()];
        for (int i = 0; i < deletedUIDS.size(); i++) {
            String key = (String) deletedUIDS.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
        }
        return keys;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker ">
    // #[regen=yes,id=DCE.EE4A388C-A94E-B384-EC8A-2E349EACBA40]
    // </editor-fold>
    public SyncItemKey[] getNewSyncItemKeys(java.sql.Timestamp sinceTs, java.sql.Timestamp untilTs) {
        log.info("getNewSyncItemKeys()");
        ArrayList newUIDS = so.getAddedToStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[newUIDS.size()];
        for (int i = 0; i < newUIDS.size(); i++) {
            String key = (String) newUIDS.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
            itemStates.put(sk, "new");
        }
        return keys;
    }

    public SyncItem getSyncItemFromId(SyncItemKey syncItemKey) throws SyncSourceException {
        String uid = syncItemKey.getKeyAsString();
        log.info("getSyncItemFromId(" + uid + ")");
        try {
            Contact object = so.getObjectFromStore(uid);
            OutboundFunambolContactObject obc = getOutboundProcessor(object);
            SyncItem si = obc.generateSyncItem(this);
            si.setState(getStateForItem(syncItemKey));
            si.setType(this.getType());
            return si;
        } catch (Exception ex) {
            log.log(Level.SEVERE, "getSyncItemFromId", ex);
            throw new SyncSourceException(ex);
        }
    }

    public void removeSyncItem(SyncItemKey syncItem, Timestamp arg1, boolean arg2) throws SyncSourceException {
        log.info("removeSyncItem(" + syncItem.getKeyAsString() + ")");
        try {
            so.deleteObject(syncItem.getKeyAsString());
        } catch (Exception e) {
            throw new SyncSourceException(e);
        }
    }

    public SyncItem updateSyncItem(SyncItem toUpdate) throws SyncSourceException {
        log.info("updateSyncItem(" + toUpdate.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolContactObject ifcb = initInboundProcessor(toUpdate);
            String output = ifcb.getStringRepresentation(Constants.TYPE_CONTACT_VCARD30);
            String name = ifcb.getObjectName();
            String uid = ifcb.getUid();
            int status = so.replaceObject(ifcb.getDestinationStore(),uid, name, output);
            return getSyncItemFromId(toUpdate.getKey());
        } catch (Exception e) {
            log.info("Exception caught in updateSyncItem");
            log.throwing(this.getClass().getName(), "updateSyncItem", e);
            throw new SyncSourceException(e);
        }
    }

    public SyncItem addSyncItem(SyncItem toAdd) throws SyncSourceException {
        log.info("addSyncItem(" + toAdd.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolContactObject ifcb = initInboundProcessor(toAdd);
            String output = ifcb.getStringRepresentation(Constants.TYPE_CONTACT_VCARD30);
            String name = ifcb.getObjectName();
            String uid = ifcb.getUid();
            String srvUid = so.addObject(ifcb.getDestinationStore(), uid, name, output);
            return getSyncItemFromId(new SyncItemKey(srvUid));
        } catch (Exception e) {
            log.info("Exception caught in addSyncItem");
            log.throwing(this.getClass().getName(), "addSyncItem", e);
            throw new SyncSourceException(e);
        }
    }

    public SyncItemKey[] getSyncItemKeysFromTwin(SyncItem syncItem) throws SyncSourceException {
        log.log(Level.INFO, "getSyncItemKeysFromTwin ("+ syncItem.getKey().getKeyAsString()+")");
        String uid = syncItem.getKey().getKeyAsString();
        String data = new String(syncItem.getContent());
        try {
            InboundFunambolContactObject ifcb = initInboundProcessor(syncItem);
            String vcName = ifcb.getObjectName();
            if (vcName == null) {
                // No name? Lets get out of here!
                return null;
            }
            String match = so.searchUids(vcName, 0,0);
            if (match != null) {
                log.log(Level.INFO, "Match found: "+ match);
                SyncItemKey[] matches = new SyncItemKey[1];
                matches[0] = new SyncItemKey(match);
                return matches;
            }
        } catch (Exception ex) {
            log.log(Level.INFO, "Error cause data="+ data);
            log.log(Level.SEVERE, "getSyncItemKeysFromTwin", ex);
            throw new SyncSourceException(ex);
        }
        return null;
    }

    public void setOperationStatus(String reason, int status, SyncItemKey[] keys) {
        for (int i = 0; i < keys.length; i++) {
            SyncItemKey syncItemKey = keys[i];
            log.info("setOperationStatus(" + reason + "," + status + "," + syncItemKey.getKeyAsString() + ")");
        }

    }

    @Override
    public void endSync() throws SyncSourceException {
        log.info("endSync()");
        try {
            so.close();
            fh.close();
        } catch (Exception e) {
            log.info("Exception caught in endSync()");
            throw new SyncSourceException(e);
        }
        super.endSync();
    }

    /**
     *  Called to get the keys of the items updated in the time frame sinceTs - untilTs.
     *   <br><code>sinceTs</code> null means all keys of the items updated until <code>untilTs</code>.
     *   <br><code>untilTs</code> null means all keys of the items updated since <code>sinceTs</code>.
     *      @param sinceTs consider the changes since this point in time.
     *   @param untilTs consider the changes until this point in time.
     *      @return an array of keys containing the <code>SyncItemKey</code>'s key of the updated
     *     items in the given time frame. It MUST NOT return null for
     *     no keys, but instad an empty array.
     */
    // <editor-fold defaultstate="collapsed" desc=" UML Marker ">
    // #[regen=yes,id=DCE.A80FDB1D-9EC6-0537-6211-5EC359BAAB6D]
    // </editor-fold>
    public SyncItemKey[] getUpdatedSyncItemKeys(Timestamp sinceTs, Timestamp untilTs) {
        log.info("getUpdatedSyncItemKeys()");
        ArrayList updatedKeys = so.getUpdatedInStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[updatedKeys.size()];
        for (int i = 0; i < updatedKeys.size(); i++) {
            String key = (String) updatedKeys.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
            itemStates.put(sk, "updated");
        }
        return keys;
    }

    public void init() throws BeanInitializationException {
    }

    private OutboundFunambolContactObject getOutboundProcessor(Contact origObject) throws Exception {
        OutboundFunambolContactObject obc = new OutboundFunambolContactObject();
        obc.setContactObject(origObject);
        return obc;
    }

    private char getStateForItem(SyncItemKey sik) {
        String state = (String) itemStates.get(sik);
        if (state == null) {
            return SyncItemState.SYNCHRONIZED;
        } else if (state.equals("new")) {
            return SyncItemState.NEW;
        } else if (state.equals("updated")) {
            return SyncItemState.UPDATED;
        }
        return SyncItemState.UNKNOWN;
    }

    public void setConnectorProperties(Properties p) {
        connectorProperties = p;
    }

    public Properties getConnectorProperties() {
        return connectorProperties;
    }

    public SyncContext getSyncContext() {
        return ctx;
    }

    /** Create the inbound object processing class (default is
     * InboundFunambolContactObject
     * @see InboundFunambolContactObject
     */
    private InboundFunambolContactObject initInboundProcessor(SyncItem si) throws Exception {
        InboundFunambolContactObject ifcb;
        ifcb = new InboundFunambolContactObject();
        ifcb.setSyncSource(this);
        ifcb.setOriginalSyncItem(si);
        return ifcb;
    }
}
