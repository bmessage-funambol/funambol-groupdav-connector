/*
 * OutboundFunambolContactObject.java
 *
 * Created on Oct 28, 2007, 1:48:43 PM
 *
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.contacts;

//import com.funambol.common.pim.common.Converter;
import com.funambol.common.pim.common.Property;
import com.funambol.common.pim.contact.BusinessDetail;
import net.bionicmessage.funambol.framework.OutboundObject;

//import com.funambol.foundation.pdi.contact.*;
//import com.funambol.foundation.pdi.converter.*;
//import com.funambol.foundation.pdi.parser.*;
import com.funambol.common.pim.contact.Contact;
import com.funambol.common.pim.contact.PersonalDetail;
import com.funambol.common.pim.converter.*;
import com.funambol.common.pim.sif.SIFCParser;
import java.io.StringReader;
import com.funambol.common.pim.vcard.*;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemImpl;
import com.funambol.framework.engine.source.SyncSource;
import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.List;
import net.bionicmessage.funambol.framework.ObjectTransformationException;

/**
 *
 * @author matt
 */
public class OutboundFunambolContactObject implements OutboundObject {

    public static final String TYPE_SIFC = "text/x-s4j-sifc";
    public static final String TYPE_VCARD = "text/x-vcard";
    private Contact conObject;

    public OutboundFunambolContactObject() {
    }

    public void setContactObject(Contact obj) {
        conObject = obj;
    }

    public Object getPDIDataObject() {
        return conObject;
    }

    public Class getPDIDataClass() {
        return com.funambol.common.pim.contact.Contact.class;
    }

    public String getStringRepresentation(String type) throws ObjectTransformationException {
        try {
            //StringReader sr = new StringReader(type);
            BaseConverter conv = null;
            if (type.equals(TYPE_SIFC)) {
                conv = new ContactToSIFC(null, "UTF-8");
            } else if (type.endsWith(TYPE_VCARD)) {
                conv = new ContactToVcard(null, "UTF-8");
            }
            removeEmptyPhonesEmails(conObject);
            return conv.convert(conObject);
        } catch (Exception e) {
            throw new ObjectTransformationException("Error transforming contact to" + type, e);
        }
    }

    public void parseIntoObjectFromString(String str, String type) throws ObjectTransformationException {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(str.getBytes("UTF-8"));
        if (type.equals(TYPE_SIFC)) {
            SIFCParser scp = new SIFCParser(bis);
            conObject = scp.parse();
        } else if (type.equals(TYPE_VCARD)) {
            VcardParser vcp = new VcardParser(bis);
            conObject = vcp.vCard();
        }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error parsing from type: "+ type, e);
        }
    }

    public String getDefaultStringOutputType() {
        return TYPE_VCARD;
    }

    public SyncItem generateSyncItem(SyncSource origSource) throws Exception {
        String content = null;
        String type = origSource.getInfo().getPreferredType().getType();
        if (type == null) {
            content = getStringRepresentation(getDefaultStringOutputType());
        } else {
            content = getStringRepresentation(type);
        }
        String uid = conObject.getUid();
        SyncItemImpl si = new SyncItemImpl(origSource, uid);
        byte[] con = content.getBytes("UTF-8");
        si.setContent(con);
        return si;
    }

    /**
     * Remove empty properties (Phone,Email) that could confuse clients
     * as they get similar types on output
     * @param propertyCollection
     */
    private void removeEmptyPhonesEmails(Contact contact) {
        PersonalDetail pd = contact.getPersonalDetail();
        removeEmptyProperties(pd.getEmails());
        removeEmptyProperties(pd.getPhones());

        BusinessDetail bd = contact.getBusinessDetail();
        removeEmptyProperties(bd.getEmails());
        removeEmptyProperties(bd.getPhones());
    }

    /**
     * Utility method used by the above
     * @param propertyCollection Collection to remove empty props
     */
    private static void removeEmptyProperties(List<Property> propertyCollection) {
        if (propertyCollection == null)
            return;
        
        Iterator<Property> propertyIterator = propertyCollection.iterator();
        while (propertyIterator.hasNext()) {
            Property p = propertyIterator.next();
            String strValue = p.getPropertyValueAsString();
            if (strValue.isEmpty()) {
                propertyIterator.remove();
                continue;
            }
        }
    }

}