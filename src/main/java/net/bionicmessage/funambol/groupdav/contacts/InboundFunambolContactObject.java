/*
 * InboundFunambolContactObject.java
 * 
 * Created on Oct 28, 2007, 1:14:16 PM
 * 
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.contacts;

import com.funambol.common.pim.common.Property;
import java.util.Properties;
import net.bionicmessage.funambol.framework.InboundObject;
import com.funambol.common.pim.contact.*;
import com.funambol.common.pim.converter.ContactToSIFC;
import com.funambol.common.pim.converter.ContactToVcard;
import com.funambol.common.pim.sif.SIFCParser;
import com.funambol.common.pim.vcard.VcardParser;
//import com.funambol.foundation.pdi.contact.*;
//import com.funambol.foundation.pdi.converter.*;
//import com.funambol.foundation.pdi.parser.*;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.server.ConvertDatePolicy;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.TimeZone;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.bionicmessage.utils.ContactToVcard3;
/**
 * Represents a Contact object in bound to ContactSyncSource
 * @author matt
 */

public class InboundFunambolContactObject implements InboundObject {

    private String destinationStore = "default";
    private Contact contactObject = null;
    private ContactSyncSource css = null;
    private String uid = null;
    private SyncItem origItem;
    
    public InboundFunambolContactObject() {

    }

    public void setSyncSource(ContactSyncSource ss) {
        css = ss;
    }

    public ContactSyncSource getSyncSource() {
        return css;
    }

    public void setOriginalSyncItem(SyncItem si) throws Exception {
        origItem = si;
        byte[] content = si.getContent();
        parseIntoObjectFromBytes(content, si.getType());
        setUid(si.getKey().getKeyAsString());
    }

    public SyncItem getOriginalSyncItem() throws Exception {
        return origItem;
    }

    public void setUid(String uid) {
        this.uid = uid;
        contactObject.setUid(uid);
    }

    public String getUid() {
        return uid;
    }

    public void setDestinationStore(String destination) {
        destinationStore = destination;
    }

    public String getDestinationStore() {
        return destinationStore;
    }

    public Object getPDIDataObject() {
        return contactObject;
    }

    public Class getPDIDataClass() {
        return Contact.class;
    }

    public String getStringRepresentation(String type) throws ObjectTransformationException {
        try {
            if (type.equals(Constants.TYPE_CONTACT_VCARD30)) {
                /* vCard 3.0 requires an FN object */
                if (Property.isEmptyProperty(contactObject.getName().getDisplayName())) {
                    StringBuilder displayNameBuilder = new StringBuilder();
                    if (!Property.isEmptyProperty(contactObject.getName().getSalutation())) {
                        displayNameBuilder.append(contactObject.getName().getSalutation().getPropertyValueAsString());
                        displayNameBuilder.append(" ");
                    }
                    if (!Property.isEmptyProperty(contactObject.getName().getFirstName())) {
                        displayNameBuilder.append(contactObject.getName().getFirstName().getPropertyValueAsString());
                        displayNameBuilder.append(" ");
                    }
                    if (!Property.isEmptyProperty(contactObject.getName().getMiddleName())) {
                        displayNameBuilder.append(contactObject.getName().getMiddleName().getPropertyValueAsString());
                        displayNameBuilder.append(" ");
                    }
                    if (!Property.isEmptyProperty(contactObject.getName().getLastName())) {
                        displayNameBuilder.append(contactObject.getName().getLastName().getPropertyValueAsString());
                        displayNameBuilder.append(" ");
                    }
                    if (!Property.isEmptyProperty(contactObject.getName().getSuffix())) {
                        displayNameBuilder.append(contactObject.getName().getSuffix().getPropertyValueAsString());
                    }
                    Property displayNameProperty = new Property(displayNameBuilder.toString());
                    contactObject.getName().setDisplayName(displayNameProperty);
                }
                ContactToVcard3 ctov3 = new ContactToVcard3(null, ContactToVcard3.CHARSET_UTF8);
                String str = ctov3.convert(contactObject);
                return str;
            } else if (type.equals(Constants.TYPE_CONTACT_VCARD21)) {
                ContactToVcard ctov = new ContactToVcard(null, ContactToVcard.CHARSET_UTF8);
                String str = ctov.convert(contactObject);
                return str;
            } else if (type.equals(Constants.TYPE_CONTACT_SIFC)) {
                ContactToSIFC ctos = new ContactToSIFC(null, ContactToVcard.CHARSET_UTF8);
                String str = ctos.convert(contactObject);
                return str;
            }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error parsing contact from " + type, e);
        }
        throw new UnsupportedOperationException("Type: " + type + " unsupported");
    }

    public void parseIntoObjectFromString(String str, String type) throws ObjectTransformationException {
        try {
            parseIntoObjectFromBytes(str.getBytes("UTF-8"), type);
        } catch (UnsupportedEncodingException e) {
            throw new ObjectTransformationException("Error outputting content in UTF-8");
        }
    }

    public void parseIntoObjectFromBytes(byte[] content, String type) throws ObjectTransformationException {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(content);
            if (type.equals(Constants.TYPE_CONTACT_VCARD21) || type.equals(Constants.TYPE_CONTACT_VCARD30)) {
                VcardParser vcp = new VcardParser(bis, null, null);
                contactObject = vcp.vCard();
            } else if (type.equals(Constants.TYPE_CONTACT_SIFC)) {
                SIFCParser sc = new SIFCParser(bis);
                contactObject = sc.parse();
            }
            PersonalDetail pd = contactObject.getPersonalDetail();
            Property textPhoto = pd.getPhoto();
            if (textPhoto != null
                    && textPhoto.getPropertyValueAsString() != null) {
                String photo = textPhoto.getPropertyValueAsString();
                photo = photo.replace("   ", "");
                textPhoto.setPropertyValue(photo);
                pd.setPhoto(textPhoto);
            }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error parsing from type: " + type, e);
        }
    }

    public String getDefaultStringOutputType() {
        return Constants.TYPE_CONTACT_VCARD21;
    }
    /** 
     * Return the vcard display name for this contact
     * @return A String with the VCard Display Name. 
     */
    public String getObjectName() {
        //if (this.contactObject.getName().getDisplayName() != null) {
        //    return this.contactObject.getName().getDisplayName().getPropertyValueAsString();
        //}
        Name n = this.contactObject.getName();
        return n.getFirstName().getPropertyValueAsString() + " " + n.getLastName().getPropertyValueAsString();
    }

}
