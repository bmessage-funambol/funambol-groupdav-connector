/* InboundFunambolNoteObject.java
 * 
 * Created on Apr 28 2009, 12:12:12 UTC
 * 
 * Copyright (C) 2009 credativ GmbH
 * 
 * GroupDAV connector for Funambol v6.5
 * Heavily based on InboundFunambolContactObject.java [Copyright (C) 2007  Mathew McBride]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.notes;

import com.funambol.common.pim.note.Note;
import net.bionicmessage.funambol.datakit.NoteConverter;
import net.bionicmessage.funambol.framework.InboundObject;
import com.funambol.common.pim.converter.BaseConverter;
import com.funambol.common.pim.converter.NoteToSIFN;
import com.funambol.common.pim.sif.SIFNParser;
import com.funambol.common.pim.vnote.VNoteParser;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.common.pim.common.Property;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.bionicmessage.funambol.groupdav.calendar.iCalSourceObject;
import net.bionicmessage.utils.QPDecode;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;

/**
 * Represents a Note object inbound to NoteSyncSource.
 * @author Marc Brockschmidt
 */
public final class InboundFunambolNoteObject extends iCalSourceObject implements InboundObject {
    /**
     * Identifier of the destination store.
     */
    private String destinationStore = "default";
    
    /**
     * Inbound note.
     */
    private Note noteObject = null;

    /**
     * Uid of this note.
     */
    private String uid = null;
    
    /**
     * Sync item (original one).
     */
    private SyncItem origItem;
    
    /**
     * Logging object used throughout the code.
     */
    private Logger log = null;
    
    /**
     * Sync context of this synchronization source.
     */
    private SyncContext syncContext;

    /** 
     * Default constructor for InboundFunambolNoteObject, creating an
     * object based on a sync item.
     * @param si incoming sync item.
     * @param l Logger object used throughout the code.
     */
    public InboundFunambolNoteObject(final SyncItem si, final Logger l) {
        origItem = si;
        log = l;
    }

    /**
     * @param id unique ID of the encapsulated note object
     */
    public void setUid(final String id) {
        this.uid = id;
        if (noteObject != null) {
            if (noteObject.getUid() != null) {
                noteObject.getUid().setPropertyValue(id);
            } else {
                noteObject.setUid(new Property(id));
            }
        } else {
            log.info("Trying to set uid " + uid + " on null note object!");
        }
    }

    /**
     * @return unique ID of the encapsulated note object
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param destination identifier of the store for this inbound object.
     */
    public void setDestinationStore(final String destination) {
        destinationStore = destination;
    }
    
    /**
     * @return identifier of the store for this inbound object.
     */
    public String getDestinationStore() {
        return destinationStore;
    }

    @Override
    public Object getPDIDataObject() {
        return noteObject;
    }

    @Override
    public Class getPDIDataClass() {
        return com.funambol.common.pim.note.Note.class;
    }

    @Override
    public String getDefaultStringOutputType() {
        return Constants.TYPE_NOTE_ICAL;
    }

    /** 
     * Return the note subject for this subject.
     * @return A String with the note subject 
     */
    public String getSummary() {
        return noteObject.getTextDescription().getPropertyValueAsString();
    }

    /**
     * @return sync context of this inbound note object
     */
    public SyncContext getSyncContext() {
        return syncContext;
    }

    /**
     * @param ctx sync context of this inbound note object
     */
    public void setSyncContext(final SyncContext ctx) {
        this.syncContext = ctx;
    }

    @Override
    public String getStringRepresentation(final String type) throws ObjectTransformationException {
        try {
            if (type.equals(Constants.TYPE_NOTE_SIFN)) {
                BaseConverter conv = new NoteToSIFN(null, BaseConverter.CHARSET_UTF8);
                return conv.convert(noteObject);
            } else if (type.endsWith(Constants.TYPE_NOTE_VNOTE)) {
                return NoteConverter.convertNote2VNote(noteObject).toString();
            } else if (type.equals(Constants.TYPE_NOTE_ICAL)) {
                return NoteConverter.convertNote2ical4j(noteObject, null).toString();
            } else if (type.equals(Constants.TYPE_NOTE_PLAIN)) {
                return NoteConverter.convertNote2Plain(noteObject);
            }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error transforming contact to" + type, e);
        }
        throw new UnsupportedOperationException("Type: " + type + " unsupported");
    }

    @Override
    public void parseIntoObjectFromString(final String str, final String type) throws ObjectTransformationException {
        try {
            parseIntoObjectFromBytes(str.getBytes("UTF-8"), type);
        } catch (UnsupportedEncodingException e) {
            throw new ObjectTransformationException("Error outputting content in UTF-8");
        }
    }
    
    /**
     * Parse byte array into a funambol Note object.
     * 
     * @param content array of bytes representing the decoded input
     * @param type data type of the input.
     * @throws ObjectTransformationException if conversion of the input fails
     */
    public void parseIntoObjectFromBytes(final byte[] content, final String type) throws ObjectTransformationException {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(content);
            if (type.equals(Constants.TYPE_NOTE_VNOTE)) {
                new VNoteParser(bis);
                noteObject = NoteConverter.convertVNote2Note(VNoteParser.VNote());
            } else if (type.equals(Constants.TYPE_NOTE_SIFN)) {
                SIFNParser sn = new SIFNParser(bis);
                noteObject = sn.parse();
            } else if (type.equals(Constants.TYPE_NOTE_ICAL)) {
                CalendarBuilder cbuild = new CalendarBuilder();
                Calendar cd = cbuild.build(bis);
                /* Decode Quoted Printables */
                Calendar decoded = QPDecode.decodeQP(cd);
                noteObject = NoteConverter.convertical4j2Note(decoded, null);
            } else if (type.equals(Constants.TYPE_NOTE_PLAIN)) {
                noteObject = NoteConverter.convertPlain2Note(
                        new String(origItem.getContent()),
                        origItem.getTimestamp().toString());                
            }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error parsing from type: " + type, e);
        }
    }
}
