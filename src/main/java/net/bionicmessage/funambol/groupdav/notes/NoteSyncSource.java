/* GroupDAV connector for Funambol
 * 
 * Created on Apr 28 2009
 * 
 * Copyright (C) 2009 credativ GmbH
 * 
 * GroupDAV connector for Funambol v6.5
 * Heavily based on CalendarSyncSource.java [Copyright (C) 2006-2008  Mathew McBride]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package net.bionicmessage.funambol.groupdav.notes;

import com.funambol.framework.core.AlertCode;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.SyncItemState;
import com.funambol.framework.engine.source.AbstractSyncSource;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSourceException;
import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.bionicmessage.utils.HTMLFormatter;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bionicmessage.funambol.datakit.NoteConverter;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.Utilities;
import net.bionicmessage.objects.MultipleSourceICalendarObjectStore;
import net.bionicmessage.objects.StoreConstants;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;

public final class NoteSyncSource extends AbstractSyncSource implements Serializable {
    /**
     * Serialization ID.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Properties of this connector.
     */
    private Properties connectorProperties = null;
    
    /**
     * storage object handling notes (as ical data) handled by this sync source.
     */
    private MultipleSourceICalendarObjectStore so = null;
    
    /**
     * File handle used for logging.
     */
    private FileHandler fh = null;
    
    /**
     * Logging object used throughout the code.
     */
    private Logger log = null;
    
    /**
     * Type of this sync (magic integer constant).
     * @see AlertCode
     */
    private int syncType = 0;
    
    /**
     * Locally used map caching the states of the synchronized items.
     */
    private Map<SyncItemKey, String> itemStates = null;
    
    /**
     * Properties of the note store.
     */
    private Properties icalStoreProperties = null;
    
    /**
     * Sync context of this synchronization source.
     */
    private SyncContext ctx;

    /**
     *  Sets up the note sync source instance.
     *      @param syncContext the syncContext to use
     *      @throws SyncSourceException in case of any error
     *      @see SyncSource
     */
    @Override
    public void beginSync(
            final SyncContext syncContext) throws SyncSourceException {
        log = Logger.getLogger(Constants.NOTE_LOGGER_NAME + "-" + System.currentTimeMillis());
        log.info("Begin sync for Note Sync Source (" + this.hashCode() + ")");
        this.ctx = syncContext;
        String principalName = Utilities.cleanFilePath(ctx.getPrincipal().getName());
        String storeDir = connectorProperties.getProperty(Constants.STOREDIR_PATH);
        File stdir = new File(storeDir + File.separatorChar + principalName + File.separatorChar);
        int storeopts = 0;
        
        try {
            /** Set up logging */
            if (!stdir.exists()) {
                stdir.mkdirs();
            }
            // Setup the logger
            HTMLFormatter hf = new HTMLFormatter();
            if (connectorProperties.getProperty(Constants.SINGLE_LOG) != null) {
                fh = new FileHandler(stdir.toString() + File.separatorChar + "connector.html");
                storeopts += MultipleSourceICalendarObjectStore.OPTION_SINGLELOG;
            } else {
                long curTime = new java.util.Date().getTime();
                fh = new FileHandler(stdir.toString() + File.separatorChar + "connector-" + curTime + ".html");
            }
            fh.setFormatter(hf);
            log.addHandler(fh);
            log.setLevel(Level.ALL);

            log.info("Beginning sync for " + principalName + " for mode=" + syncType);
            if (syncType == AlertCode.SLOW || syncType == AlertCode.REFRESH_FROM_SERVER) {
                storeopts += MultipleSourceICalendarObjectStore.OPTION_PURGE;
            }
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "beginSync", e);
            throw new SyncSourceException(e);
        }
        // Perform setup
        itemStates = new Hashtable<SyncItemKey, String>();
        syncType = ctx.getSyncMode();
        String creds = ctx.getPrincipal().getEncodedCredentials();
        icalStoreProperties = new Properties();
        /** Todo: simply pass connectorProperties into icalStoreProperties */
        // Process the sources
        Iterator configKeys = connectorProperties.keySet().iterator();
        while (configKeys.hasNext()) {
            String keyName = (String) configKeys.next();
            if (keyName.contains(Constants.SOURCE_LOCATION_BASE)) {
                String sourceLocation = connectorProperties.getProperty(keyName);
                sourceLocation = sourceLocation.replace("%USER%", syncContext.getPrincipal().getUsername());
                String sourceName = StoreConstants.BASE_PROPERTY_SOURCE.concat(keyName.replace(Constants.SOURCE_LOCATION_BASE, ""));
                icalStoreProperties.setProperty(sourceName, sourceLocation);
            }
        }
        String calDavComp = connectorProperties.getProperty(Constants.CALDAV_COMPONENT);
        String calDavProp = connectorProperties.getProperty(Constants.CALDAV_PROPERTY);
        String calDavStart = connectorProperties.getProperty(Constants.CALDAV_START);
        String calDavEnd = connectorProperties.getProperty(Constants.CALDAV_END);
        if (calDavComp != null) {
            icalStoreProperties.setProperty(Constants.CALDAV_COMPONENT, calDavComp);
            icalStoreProperties.setProperty(Constants.CALDAV_PROPERTY, calDavProp);
            icalStoreProperties.setProperty(Constants.CALDAV_START, calDavStart);
            icalStoreProperties.setProperty(Constants.CALDAV_END, calDavEnd);
        }
        String collectionMode = connectorProperties.getProperty(StoreConstants.PROPERTY_SERVER_MODE);
        if (collectionMode != null) {
            icalStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER_MODE, collectionMode);
        }
        String cthreads = connectorProperties.getProperty(StoreConstants.PROPERTY_SERVER_CTHREADS);
        icalStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER_CTHREADS, cthreads);
        String citems = connectorProperties.getProperty(StoreConstants.PROPERTY_SERVER_ITEMS);
        icalStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER_ITEMS, citems);
        // Handle %DOMAIN%
        String domainString = connectorProperties.getProperty(Constants.SERVER_HOST);

        // Parse users email address
        if (domainString.contains(Constants.DOMAIN_REPLACE)) {
            String emailAddr = ctx.getPrincipal().getUser().getUsername();
            if (!emailAddr.contains("@")) {
                emailAddr = ctx.getPrincipal().getUser().getEmail();
            }
            String[] emailComponenets = emailAddr.split("@");
            String domain = emailComponenets[1];
            domainString = domainString.replace(Constants.DOMAIN_REPLACE, domain);
        }
        icalStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER,
                domainString);

        icalStoreProperties.setProperty(StoreConstants.PROPERTY_STORE_LOCATION, storeDir);

        // Create store
        if (so == null) {
            List<String> neededIcalComps = new ArrayList<String>();
            neededIcalComps.add(Component.VJOURNAL);
            so = new MultipleSourceICalendarObjectStore(stdir.toString(), storeopts, neededIcalComps);
            so.setProperties(icalStoreProperties);
            so.loadSourcesFromProps();
        } else {
            log.fine("Reusing existing object store");
        }
        try {
            so.setServer(new URI(domainString), creds);
            // Sync
            so.startSync();
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "getSyncItemKeysFromTwin", e);
            throw new SyncSourceException(e);
        }
    }

    @Override
    public void endSync() throws SyncSourceException {
        try {
            log.info("endSync");
            so.printDebugReport();
            so.close();
            fh.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.throwing(this.getClass().getName(), "endSync", ex);
            throw new SyncSourceException(ex);
        }
    }

    // -------------------------------------------------------- Abstract methods

    @Override
    public SyncItemKey[] getAllSyncItemKeys() {
        log.fine("getAllSyncItemKeys()");
        SyncItemKey[] newKeys = this.getNewSyncItemKeys(null, null);
        SyncItemKey[] updatedKeys = this.getUpdatedSyncItemKeys(null, null);
        SyncItemKey[] allKnownKeys = new SyncItemKey[newKeys.length + updatedKeys.length];
        int curKey = 0;
        for (int i = 0; i < newKeys.length; i++) {
            allKnownKeys[curKey] = newKeys[i];
            curKey++;
        }
        for (int i = 0; i < updatedKeys.length; i++) {
            allKnownKeys[curKey] = updatedKeys[i];
            curKey++;
        }
        return allKnownKeys;
    }

    @Override
    public SyncItemKey[] getDeletedSyncItemKeys(
            final java.sql.Timestamp sinceTs,
            final java.sql.Timestamp untilTs) {
        log.info("getDeletedSyncItemKeys()");
        ArrayList deletedUIDS = so.getDeletedFromStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[deletedUIDS.size()];
        for (int i = 0; i < deletedUIDS.size(); i++) {
            String key = (String) deletedUIDS.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
        }
        return keys;
    }

    @Override
    public SyncItemKey[] getNewSyncItemKeys(
            final java.sql.Timestamp sinceTs,
            final java.sql.Timestamp untilTs) {
        log.info("getNewSyncItemKeys()");
        ArrayList newUIDS = so.getAddedToStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[newUIDS.size()];
        for (int i = 0; i < newUIDS.size(); i++) {
            String key = (String) newUIDS.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
            itemStates.put(sk, "new");
        }
        return keys;
    }

    @Override
    public SyncItem getSyncItemFromId(
            final SyncItemKey syncItemKey) throws SyncSourceException {
        String uid = syncItemKey.getKeyAsString();
        log.info("getSyncItemFromId(" + uid + ")");
        try {
            Calendar object = so.getObjectFromStore(uid);
            //Only handle actual notes here:
            if (object.getComponents().getComponent(Component.VJOURNAL) != null) {
                OutboundFunambolNoteObject ofc = new OutboundFunambolNoteObject(NoteConverter.convertical4j2Note(object, ctx), log);

                ofc.setConnectorProperties(this.getConnectorProperties());
                SyncItem si = ofc.generateSyncItem(this);
                si.setState(getStateForItem(syncItemKey));
                si.setType(this.getType());
                return si;
            } else {
                return null;
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "getSyncItemFromId", ex);
            throw new SyncSourceException(ex);
        }
    }

    @Override
    public void removeSyncItem(
            final SyncItemKey syncItemKey,
            final Timestamp time,
            final boolean softDelete) throws SyncSourceException {
        log.info("removeSyncItem(" + syncItemKey.getKeyAsString() + ")");
        try {
            so.deleteObject(syncItemKey.getKeyAsString());
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "removeSyncItem", ex);
            throw new SyncSourceException(ex);
        }
    }

    @Override
    public SyncItemKey[] getSyncItemKeysFromTwin(
            final SyncItem syncItem) throws SyncSourceException {
        log.info("getSyncItemKeysFromTwin(" + syncItem.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolNoteObject ifno = initInboundProcessor(syncItem);
            String uid = (String) syncItem.getKey().getKeyValue();
            ifno.parseIntoObjectFromBytes(syncItem.getContent(), syncItem.getType());
            ifno.setUid(uid);
            String summary = ifno.getSummary();
            String match = so.searchUids(summary, 0, 0);
            if (match != null) {
                log.info("Match found: " + match);
                SyncItemKey[] ma = {new SyncItemKey(match)};
                return ma;
            }
            return new SyncItemKey[0];
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "getSyncItemKeysFromTwin", ex);
            throw new SyncSourceException(ex);
        }
    }

    /**
     *  Called to get the keys of the items updated.
     *      @param unusedArg1 timestamp forced on us by the interface (not used).
     *      @param unusedArg2 timestamp forced on us by the interface (not used).
     *      @return an array of keys containing the <code>SyncItemKey</code>'s key of the updated
     *       items
     */
    public SyncItemKey[] getUpdatedSyncItemKeys(final Timestamp unusedArg1, final Timestamp unusedArg2) {
        log.info("getUpdatedSyncItemKeys()");
        ArrayList updatedKeys = so.getUpdatedInStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[updatedKeys.size()];
        for (int i = 0; i < updatedKeys.size(); i++) {
            String key = (String) updatedKeys.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
            itemStates.put(sk, "updated");
            log.info("Updated sync item key: " + sk.toString());
        }
        return keys;
    }

    /**
     * @return properties of this connector.
     */
    public Properties getConnectorProperties() {
        return connectorProperties;
    }

    /**
     * @param p Set properties of this connector.
     */
    public void setConnectorProperties(final Properties p) {
        this.connectorProperties = p;
    }

    @Override
    public SyncItem updateSyncItem(final SyncItem arg0) throws SyncSourceException {
        log.info("updateSyncItem(" + arg0.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolNoteObject ifno = initInboundProcessor(arg0);
            ifno.parseIntoObjectFromBytes(arg0.getContent(), arg0.getType());
            String uid = arg0.getKey().getKeyAsString();
            ifno.setUid(uid);
            so.deleteObject(ifno.getUid());
            so.addObject(ifno.getDestinationStore(),
                    ifno.getUid(),
                    ifno.getSummary(),
                    ifno.getStringRepresentation(Constants.TYPE_NOTE_ICAL),
                    0, 0);
            return getSyncItemFromId(arg0.getKey());
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "updateSyncItem", ex);
            throw new SyncSourceException(ex);
        }
    }

    @Override
    public SyncItem addSyncItem(final SyncItem arg0) throws SyncSourceException {
        log.info("addSyncItem(" + arg0.getKey().getKeyAsString() + ")");
        try {
            String input = "";
            for (byte b : arg0.getContent()) { input += (char) b; }
            InboundFunambolNoteObject ifno = initInboundProcessor(arg0);
            ifno.parseIntoObjectFromBytes(arg0.getContent(), arg0.getType());
            String uid = (String) arg0.getKey().getKeyValue();
            ifno.setUid(uid);
            String newUID = so.addObject(ifno.getDestinationStore(),
                    ifno.getUid(),
                    ifno.getSummary(),
                    ifno.getStringRepresentation(Constants.TYPE_NOTE_ICAL),
                    0, 0);
            return getSyncItemFromId(new SyncItemKey(newUID));
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "addSyncItem", ex);
            throw new SyncSourceException(ex);
        }
    }

    @Override
    public void setOperationStatus(final String operationName, final int status, final SyncItemKey[] keys) {
        for (int i = 0; i < keys.length; i++) {
            SyncItemKey syncItemKey = keys[i];
            log.finer("setOperationStatus(" + operationName + "," + status + "," + syncItemKey.getKeyAsString() + ")");
        }
    }

    /**
     * @param sik a key identifying an item to sync
     * @return char indicating the state of an item to sync (either SYNCED/NEW/UPDATED/...)
     */
    private char getStateForItem(final SyncItemKey sik) {
        String state = (String) itemStates.get(sik);
        if (state == null) {
            return SyncItemState.SYNCHRONIZED;
        } else if (state.equals("new")) {
            return SyncItemState.NEW;
        } else if (state.equals("updated")) {
            return SyncItemState.UPDATED;
        }
        return SyncItemState.UNKNOWN;
    }

    /** 
     * Create the inbound object processing class.
     * @param si inbound sync item to process
     * @return InboundFunambolNoteObject
     */
    private InboundFunambolNoteObject initInboundProcessor(final SyncItem si) {
        InboundFunambolNoteObject ifno = new InboundFunambolNoteObject(si, log);
        ifno.setConnectorProperties(connectorProperties);
        ifno.setSyncContext(ctx);
        return ifno;
    }

    public void init() {
    }
}
