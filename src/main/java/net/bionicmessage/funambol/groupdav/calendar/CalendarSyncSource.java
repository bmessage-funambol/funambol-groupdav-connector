/* GroupDAV connector for Funambol
 * Copyright (C) 2006-2011 Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package net.bionicmessage.funambol.groupdav.calendar;

import com.funambol.framework.core.CTInfo;
import com.funambol.framework.core.DataStore;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.SyncItemState;
import com.funambol.framework.engine.source.AbstractSyncSource;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSourceException;
import com.funambol.framework.server.Sync4jDevice;
import com.funambol.framework.tools.beans.*;
import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.naming.NamingException;
import net.bionicmessage.utils.HTMLFormatter;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bionicmessage.funambol.UserIdMappingUtil;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.Utilities;
import net.bionicmessage.funambol.groupdav.calendar.plugins.MixedCalendarTaskInbound;
import net.bionicmessage.objects.MultipleSourceICalendarObjectStore;
import net.bionicmessage.objects.StoreConstants;
import net.fortuna.ical4j.model.Calendar;


public class CalendarSyncSource extends AbstractSyncSource implements LazyInitBean, Serializable {

    private Properties connectorProperties = null;
    private SyncContext ctx = null;
    private FileHandler fh = null;
    Logger log = null;
    private int syncType = 0;
    private Map itemStates = null;
    private MultipleSourceICalendarObjectStore so = null;

    private Properties icalStoreProperties = null;

    private String mappedUserId = null;
    private boolean mapUserId = false;

    /**
     *  Sets up the calendar sync source instance
     *      @param syncContext the syncContext to use
     *      @throws SyncSourceException in case of any error
     *      @see SyncSource
     */
    @Override
    public void beginSync(SyncContext syncContext) throws SyncSourceException {
        log = Logger.getLogger(Constants.CALENDAR_LOGGER_NAME + "-" + System.currentTimeMillis());
        log.log(Level.INFO, "Begin sync for Calendar Sync Source ({0})", this.hashCode());
        this.ctx = syncContext;
        String principalName = Utilities.cleanFilePath(ctx.getPrincipal().getName());
        String storeDir = connectorProperties.getProperty(Constants.STOREDIR_PATH);
        File stdir = new File(storeDir + File.separatorChar + principalName + File.separatorChar);
        int storeopts = 0;
        try {
            /** Set up logging */
            if (!stdir.exists()) {
                stdir.mkdirs();
            }
            // Setup the logger
            HTMLFormatter hf = new HTMLFormatter();
            if (connectorProperties.getProperty(Constants.SINGLE_LOG) != null) {
                fh = new FileHandler(stdir.toString() + File.separatorChar + "connector.html");
                storeopts += MultipleSourceICalendarObjectStore.OPTION_SINGLELOG;
            } else {
                long curTime = new java.util.Date().getTime();
                fh = new FileHandler(stdir.toString() + File.separatorChar + "connector-" + curTime + ".html");
            }
            fh.setFormatter(hf);
            log.addHandler(fh);
            log.setLevel(Level.ALL);

            itemStates = new Hashtable();
            syncType = ctx.getSyncMode();
            log.info("Beginning sync for " + principalName + " for mode=" + syncType);
            if (syncType == 201 || syncType == 205) {
                storeopts += 64;
            }
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "beginSync", e);
            throw new SyncSourceException(e);
        }
        // Perform setup
        itemStates = new Hashtable();
        syncType = ctx.getSyncMode();
        String creds = ctx.getPrincipal().getEncodedCredentials();
        icalStoreProperties = new Properties();
        /** Todo: simply pass connectorProperties into icalStoreProperties */
        // Process the sources

        if ("true".equals(connectorProperties.get(Constants.REPLACE_USER_ID))) {
            mapUserId = true;
            try {
                mappedUserId = UserIdMappingUtil.getIdForUser(syncContext.getPrincipal().getUsername());
            } catch (SQLException ex) {
                log.throwing(this.getClass().getName(), "beginSync", ex);
                throw new SyncSourceException("[User ID Mapping] cannot establish database connection",ex);
            } catch (NamingException ex) {
                log.throwing(this.getClass().getName(), "beginSync", ex);
                throw new SyncSourceException("[User ID Mapping] could not obtain JNDI service",ex);
            }
        }

        String defaultSourceLocation = null;
        Iterator configKeys = connectorProperties.keySet().iterator();
        while (configKeys.hasNext()) {
            String keyName = (String) configKeys.next();
            if (keyName.contains(Constants.SOURCE_LOCATION_BASE)) {
                String sourceLocation = connectorProperties.getProperty(keyName);
                sourceLocation = sourceLocation.replace("%USER%", syncContext.getPrincipal().getUsername());
                
                if (mapUserId)
                    sourceLocation = sourceLocation.replace("%USERID%", mappedUserId);

                String sourceName = StoreConstants.BASE_PROPERTY_SOURCE.concat(keyName.replace(Constants.SOURCE_LOCATION_BASE, ""));
                if ("default".equals(sourceName))
                    defaultSourceLocation = sourceLocation;
                
                icalStoreProperties.setProperty(sourceName, sourceLocation);
            }
        }
        /* String calDavComp = connectorProperties.getProperty(Constants.CALDAV_COMPONENT);
        String calDavProp = connectorProperties.getProperty(Constants.CALDAV_PROPERTY);
        String calDavStart = connectorProperties.getProperty(Constants.CALDAV_START);
        String calDavEnd = connectorProperties.getProperty(Constants.CALDAV_END);
        if (calDavComp != null) {
            icalStoreProperties.setProperty(Constants.CALDAV_COMPONENT, calDavComp);
            icalStoreProperties.setProperty(Constants.CALDAV_PROPERTY, calDavProp);
            icalStoreProperties.setProperty(Constants.CALDAV_START, calDavStart);
            icalStoreProperties.setProperty(Constants.CALDAV_END, calDavEnd);
        } */
        
        // Handle %DOMAIN%
        String domainString = connectorProperties.getProperty(Constants.SERVER_HOST);

        // Parse users email address
        if (domainString.contains(Constants.DOMAIN_REPLACE)) {
            String emailAddr = ctx.getPrincipal().getUser().getUsername();
            if (!emailAddr.contains("@")) {
                emailAddr = ctx.getPrincipal().getUser().getEmail();
            }
            String[] emailComponenets = emailAddr.split("@");
            String domain = emailComponenets[1];
            domainString = domainString.replace(Constants.DOMAIN_REPLACE, domain);
        }

        
        icalStoreProperties.setProperty(StoreConstants.PROPERTY_SERVER,
                domainString);

        icalStoreProperties.setProperty(StoreConstants.PROPERTY_STORE_LOCATION, storeDir);

        // Create store
        if (so == null) {
            so = new MultipleSourceICalendarObjectStore(stdir.toString(), storeopts);
            so.setProperties(icalStoreProperties);
            so.loadSourcesFromProps();
        } else {
            log.info("Reusing existing object store");
        }
        try {
            so.setServer(new URI(domainString), creds);
            // Sync
            so.startSync();
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "startSync", e);
            throw new SyncSourceException(e);
        }
    }

    /**
     *  It logs the howMany counters. It should then be called by extending
     *   classes before anything else.
     *      @param principal user/device
     *      @throws SyncSourceException in case of any error
     *      @see SyncSource
     */
    @Override
    public void endSync() throws SyncSourceException {
        try {
            log.info("endSync");
            so.printDebugReport();
            so.close();
            fh.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.throwing(this.getClass().getName(), "endSync", ex);
            throw new SyncSourceException(ex);
        }
    }

    /**
     *  -------------------------------------------------------- Abstract methods
     */
    public SyncItemKey[] getAllSyncItemKeys() {
        log.fine("getAllSyncItemKeys()");
        SyncItemKey[] newKeys = this.getNewSyncItemKeys(null, null);
        SyncItemKey[] updatedKeys = this.getUpdatedSyncItemKeys(null, null);
        SyncItemKey[] allKnownKeys = new SyncItemKey[newKeys.length + updatedKeys.length];
        int curKey = 0;
        for (int i = 0; i < newKeys.length; i++) {
            allKnownKeys[curKey] = newKeys[i];
            curKey++;
        }
        for (int i = 0; i < updatedKeys.length; i++) {
            allKnownKeys[curKey] = updatedKeys[i];
            curKey++;
        }
        return allKnownKeys;
    }

    /** 
     * Obtain a list of deleted (on GroupDAV Server) objects
     * @param sinceTs From time, only used for filtering (not in use here)
     * @param untilTs Until time, only used for filtering (not in use here)
     * @return
     */
    public SyncItemKey[] getDeletedSyncItemKeys(java.sql.Timestamp sinceTs, java.sql.Timestamp untilTs) {
        log.info("getDeletedSyncItemKeys()");
        ArrayList deletedUIDS = so.getDeletedFromStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[deletedUIDS.size()];
        for (int i = 0; i < deletedUIDS.size(); i++) {
            String key = (String) deletedUIDS.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
        }
        return keys;
    }

    /** 
     * Obtain a list of new objects on server
     * @param sinceTs From time, not applicable
     * @param untilTs To time, not applicable
     * @return
     */
    public SyncItemKey[] getNewSyncItemKeys(java.sql.Timestamp sinceTs, java.sql.Timestamp untilTs) {
        log.info("getNewSyncItemKeys()");
        ArrayList newUIDS = so.getAddedToStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[newUIDS.size()];
        for (int i = 0; i < newUIDS.size(); i++) {
            String key = (String) newUIDS.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
            itemStates.put(sk, "new");
        }
        return keys;
    }

    public SyncItem getSyncItemFromId(SyncItemKey syncItemKey) throws SyncSourceException {
        String uid = syncItemKey.getKeyAsString();
        
        log.log(Level.INFO, "getSyncItemFromId("+syncItemKey.getKeyAsString()+")");

        try {
            Calendar object = so.getObjectFromStore(uid);
            OutboundFunambolCalendarObject ofc = new OutboundFunambolCalendarObject(object);
            ofc.setConnectorProperties(this.getConnectorProperties());
            SyncItem si = ofc.generateSyncItem(this);
            si.setState(getStateForItem(syncItemKey));
            si.setType(this.getType());
            return si;
        } catch (Exception ex) {
            log.log(Level.SEVERE, "getSyncItemFromId", ex);
            throw new SyncSourceException(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker ">
    // #[regen=yes,id=DCE.31A4119B-818A-A6B4-38C1-161C22D749F3]
    // </editor-fold>
    public void removeSyncItem(SyncItemKey syncItemKey, Timestamp time, boolean softDelete) throws SyncSourceException {
        log.info("removeSyncItem(" + syncItemKey.getKeyAsString() + ")");
        try {
            so.deleteObject(syncItemKey.getKeyAsString());
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "removeSyncItem", ex);
            throw new SyncSourceException(ex);
        }
    }

    /** 
     * Parse the specified sync item to find any matches on the server.
     * @param syncItem Sync item to parse and search for
     * @return An array of SyncItemKeys[] with matching data.
     * @throws com.funambol.framework.engine.source.SyncSourceException
     */
    public SyncItemKey[] getSyncItemKeysFromTwin(SyncItem syncItem) throws SyncSourceException {
        log.info("getSyncItemKeysFromTwin(" + syncItem.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolCalendarObject ifcb = initInboundProcessor(syncItem);
            ifcb.parseIntoObjectFromString(null, syncItem.getType());
            String uid = (String) syncItem.getKey().getKeyValue();
            ifcb.setUid(uid);
            String summary = ifcb.getSummary();
            long dtstart = ifcb.getDtStart();
            long dtend = ifcb.getDtEnd();
            String match = so.searchUids(summary, dtstart, dtend);
            if (match != null) {
                log.info("Match found: " + match);
                SyncItemKey[] ma = {new SyncItemKey(match)};
                return ma;
            }
            return new SyncItemKey[0];
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "getSyncItemKeysFromTwin", ex);
            throw new SyncSourceException(ex);
        }
    }

    /**
     *  Called to get the keys of the items updated in the time frame sinceTs - untilTs.
     *   <br><code>sinceTs</code> null means all keys of the items updated until <code>untilTs</code>.
     *   <br><code>untilTs</code> null means all keys of the items updated since <code>sinceTs</code>.
     *      @param sinceTs consider the changes since this point in time.
     *   @param untilTs consider the changes until this point in time.
     *      @return an array of keys containing the <code>SyncItemKey</code>'s key of the updated
     *     items in the given time frame. It MUST NOT return null for
     *     no keys, but instad an empty array.
     */
    public SyncItemKey[] getUpdatedSyncItemKeys(Timestamp sinceTs, Timestamp untilTs) {
        log.info("getUpdatedSyncItemKeys()");
        ArrayList updatedKeys = so.getUpdatedInStoreUIDS();
        SyncItemKey[] keys = new SyncItemKey[updatedKeys.size()];
        for (int i = 0; i < updatedKeys.size(); i++) {
            String key = (String) updatedKeys.get(i);
            SyncItemKey sk = new SyncItemKey(key);
            keys[i] = sk;
            itemStates.put(sk, "updated");
        }
        return keys;
    }

    public Properties getConnectorProperties() {
        return connectorProperties;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker ">
    // #[regen=yes,regenBody=yes,id=DCE.F282BB88-7C8F-0167-DA12-B87014A3DD56]
    // </editor-fold>
    public void setConnectorProperties(Properties val) {
        this.connectorProperties = val;
    }

    public SyncItem updateSyncItem(SyncItem key) throws SyncSourceException {
        log.info("updateSyncItem(" + key.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolCalendarObject ifcb = initInboundProcessor(key);
            String uid = (String) key.getKey().getKeyValue();
            ifcb.parseIntoObjectFromString(null, key.getType());
            ifcb.setUid(uid);
            Calendar oldCal = so.getObjectFromStore(uid);
            /* Some update only operations when attributes need to be
             * grabbed from the stored calendar */
            ifcb.copyOrganizerFromCalendar(oldCal);
            ifcb.copyAttendeesFromCalendar(oldCal);
            ifcb.copyVAlarmsFromCalendar(oldCal);
            Calendar cal = (Calendar) ifcb.getPDIDataObject();
            String itemUID = ifcb.getUID();
            if (!itemUID.equals(uid)) {
                ifcb.setUid(uid);
            }
            Calendar updated = this.so.updateCalendarObject(ifcb.getDestinationStore(), uid, cal);
            OutboundFunambolCalendarObject ofc = new OutboundFunambolCalendarObject(updated);
            ofc.setConnectorProperties(getConnectorProperties());
            SyncItem si = ofc.generateSyncItem(this);
            si.setState(getStateForItem(key.getKey()));
            si.setType(getType());
            return si;
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "updateSyncItem", ex);
            throw new SyncSourceException(ex);
        }
    }

    public SyncItem addSyncItem(SyncItem addItem) throws SyncSourceException {
        log.info("addSyncItem(" + addItem.getKey().getKeyAsString() + ")");
        try {
            InboundFunambolCalendarObject ifcb = initInboundProcessor(addItem);
            ifcb.parseIntoObjectFromString(null, addItem.getType());
            String uid = (String) addItem.getKey().getKeyValue();
            ifcb.setUid(uid);
            net.fortuna.ical4j.model.Calendar cal = (net.fortuna.ical4j.model.Calendar) ifcb.getPDIDataObject();
            String newUID = so.addObject(ifcb.getDestinationStore(), ifcb.getUID(), ifcb.getSummary(), ifcb.getStringRepresentation("text/calendar"), ifcb.getDtStart(), ifcb.getDtEnd());
            return getSyncItemFromId(new SyncItemKey(newUID));
        } catch (Exception ex) {
            log.throwing(this.getClass().getName(), "addSyncItem", ex);
            throw new SyncSourceException(ex);
        }
    }

    public void setOperationStatus(String arg0, int arg1, SyncItemKey[] arg2) {
        for (int i = 0; i < arg2.length; i++) {
            SyncItemKey syncItemKey = arg2[i];
            log.info("setOperationStatus(" + arg0 + "," + arg1 + "," + syncItemKey.getKeyAsString() + ")");
        }
    }

    private char getStateForItem(SyncItemKey sik) {
        String state = (String) itemStates.get(sik);
        if (state == null) {
            return SyncItemState.SYNCHRONIZED;
        } else if (state.equals("new")) {
            return SyncItemState.NEW;
        } else if (state.equals("updated")) {
            return SyncItemState.UPDATED;
        }
        return SyncItemState.UNKNOWN;
    }

    /** Create the inbound object processing class (default is 
     * InboundFunambolCalendarObject). If the administrator has their own
     * extension of InboundFunambolCalendarObject loaded, use that instead */
    private InboundFunambolCalendarObject initInboundProcessor(SyncItem si) throws BeanInstantiationException, BeanInitializationException, BeanException {
        InboundFunambolCalendarObject ifcb;
       /* if (connectorProperties.getProperty(Constants.INBOUND_PROCESSOR) != null) {
            Object processor = BeanFactory.getBeanInstanceFromConfig(
                    connectorProperties.getProperty(Constants.INBOUND_PROCESSOR));
            ifcb = (InboundFunambolCalendarObject) processor;
            ifcb.setItem(si);
        } else */ if (connectorProperties.getProperty(Constants.MIXED_MODE) != null) {
            ifcb = new MixedCalendarTaskInbound();
            ifcb.setItem(si);
        } else {
            ifcb = new InboundFunambolCalendarObject(si);
        }
        ifcb.setConnectorProperties(connectorProperties);
        ifcb.setSyncContext(ctx);

        // Get the device timezone for correct recurrence conversion
        Sync4jDevice s4d = ctx.getPrincipal().getDevice();
        String devTz = s4d.getTimeZone();
        if (devTz != null && devTz.length() > 0) {
            TimeZone tz = TimeZone.getTimeZone(devTz);
            ifcb.setDeviceTimeZone(tz);
        }
        return ifcb;
    }

    public void init() throws BeanInitializationException {
    }
    /**
    * Finds the preferred RX content type, looking through all the datastores.
    * 
    * @param context the SyncContext of the current synchronization session.
    * @return a string containing the preferred MIME type ("text/x-vcalendar" or
    *         ("text/calendar"), or null if no preferred MIME type could be 
    *         found out.
    */
   private String findRXContentType(SyncContext context, boolean isTask) {
       List<DataStore> dataStores;              
       try {
        dataStores = context.getPrincipal()
                            .getDevice()
                            .getCapabilities()
                            .getDevInf()
                            .getDataStores();       
       } catch (NullPointerException e) { // something is missing
           return null;
       }
       if (dataStores == null) {
           return null;
       }
       String sifType = Constants.TYPE_CALENDAR_SIF;
       if (isTask) {
           sifType = Constants.TYPE_TASK_SIF;
       }
       boolean xvCalendar = false;
       boolean sifc = false;
       for (DataStore dataStore : dataStores) {
           CTInfo rxPref = dataStore.getRxPref();
           if (rxPref != null) {
               if (Constants.TYPE_CALENDAR_VCAL.equals(rxPref.getCTType())) {
                   xvCalendar = true;
               } else if (sifType.equals(rxPref.getCTType())) {
                   sifc = true;
               }
           }
           if (xvCalendar && sifc) {
               break; // It's useless to cycle again
           }
       }
       if (xvCalendar && !sifc) {
           return Constants.TYPE_CALENDAR_VCAL; // "text/x-vcalendar"
       } else if (!xvCalendar && sifc) {
           return Constants.TYPE_CALENDAR_SIF;
       }
       
       return null;
   }
}