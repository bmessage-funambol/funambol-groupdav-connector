/*
 * DualSourceCategorySwitch.java
 *
 * Created on Oct 21, 2007, 10:12:18 PM
 *
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.calendar.plugins;

import java.io.Serializable;
import java.util.Hashtable;
import net.bionicmessage.funambol.groupdav.calendar.InboundFunambolCalendarObject;

/**
 * An extension of InboundFunambolCalendarObject which facilitates the use
 * of two GroupDAV sources, presented on the client as categories.
 * 
 * This class is a prototype intended to demonstrate a potential pipeline plugin
 * @author Mathew McBride
 */
public class InboundDualSourceCategorySwitch extends InboundFunambolCalendarObject implements Serializable {

    /* This table holds a map of user categories (what they see on their
    device) to configured groupdav sources.
    As there is a mandatory requirement for a 'default' source,
    anything not configured here/no category specified simply flows into that.
    Format:
    Client Name | Server Name
     */
    private Hashtable<String, String> substitutions = null;

    public InboundDualSourceCategorySwitch() {
        super();
    }

    @Override
    public String getDestinationStore() {
        if (substitutions != null) {
            String cat = this.getEventCategory();
            if (cat != null) {
                if (substitutions.get(cat) != null) {
                    return substitutions.get(cat);
                }
            }
        }
        return super.getDestinationStore();
    }
}