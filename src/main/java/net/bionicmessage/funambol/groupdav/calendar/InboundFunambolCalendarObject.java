/* GroupDAV connector for Funambol
 * Copyright (C) 2007-2011  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package net.bionicmessage.funambol.groupdav.calendar;

import com.funambol.common.pim.common.Property;
import net.bionicmessage.funambol.framework.InboundObject;
import com.funambol.framework.engine.*;
import net.bionicmessage.funambol.framework.Constants;

import net.fortuna.ical4j.model.Calendar;

import com.funambol.common.pim.converter.*;

import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.model.VCalendarContent;
import com.funambol.common.pim.sif.SIFCalendarParser;
import com.funambol.common.pim.xvcalendar.XVCalendarParser;
import com.funambol.framework.engine.source.SyncContext;
import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import net.bionicmessage.funambol.datakit.funambol2ical4j;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.PropertyList;
import net.fortuna.ical4j.model.component.CalendarComponent;
import net.fortuna.ical4j.model.component.VToDo;
import net.fortuna.ical4j.model.property.Action;
import net.fortuna.ical4j.model.property.Attendee;
import net.fortuna.ical4j.model.property.Categories;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Uid;

/** InboundFunambolCalendarObject represents an event/todo being processed 
 * by the sync source for addition to the server
 * @author matt
 */
public class InboundFunambolCalendarObject extends iCalSourceObject implements InboundObject {

    String destinationStore = "default";
    Calendar ical2data = null;
    TimeZone deviceTimeZone = null;
 
    private SyncItem item = null;
    SyncContext ctx = null;
    String uid = null;

    public InboundFunambolCalendarObject(SyncItem item) {
        this.item = item;
    }
    /* Null constructor for beans */

    public InboundFunambolCalendarObject() {
    }

    public void setDestinationStore(String destination) {
        destinationStore = destination;
    }

    /** 
     * Get the destination store on the server where this 
     * event will be posted
     * @return Destination store on server
     */
    public String getDestinationStore() {
        return destinationStore;
    }

    /**
     * Return the ical4j object
     * @return
     */
    public Object getPDIDataObject() {
        return ical2data;
    }

    /** 
     * Return the ical4j Calendar class (for reflection purposes)
     * @return 
     */
    public Class getPDIDataClass() {
        return ical2data.getClass();
    }

    /** 
     * Return a string representation of the event represented
     * @param type &quot;text/calendar&quot; only 
     * @return An iCalendar 2.0 string representation of this event
     * @throws net.bionicmessage.funambol.framework.ObjectTransformationException
     */
    public String getStringRepresentation(String type) throws ObjectTransformationException {
        if (type.equals("text/icalendar") || type.equals("text/calendar")) {
            return ical2data.toString();
        }
        throw new UnsupportedOperationException("Type " + type + " unsupported");
    }

    /** Parses the object from a String. 
     * 
     * @param str String containing object content. If str is null, it will be
     *  retrieved from the stored sync item.
     * @param type The Content-Type of the String, i.e text/x-vcalendar 
     * @throws net.bionicmessage.funambol.framework.ObjectTransformationException 
     *  if parse fails. 
     */
    public void parseIntoObjectFromString(String str, String type) throws ObjectTransformationException {
        String debugString = null;
        try {
            byte[] data;
            if (str == null) {
                debugString = new String(item.getContent(), "UTF-8");
                data = item.getContent();
            } else {
                debugString = str;
                data = str.getBytes("UTF-8");
            }
            // Have a debug string
            debugString = debugString.replace("\r", "xR").replace("\n", "xN");
            /* com.funambol.common.pim.calendar.Calendar fnblcal = null;
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            if (!type.equals(Constants.TYPE_CALENDAR_2445)) {
                if (type.equals(Constants.TYPE_CALENDAR_VCAL)) {
                    XVCalendarParser xvp = new XVCalendarParser(bis);
                    VCalendar vc = xvp.XVCalendar();
                    VCalendarConverter vcf = new VCalendarConverter(deviceTimeZone, "UTF-8");
                    fnblcal = vcf.vcalendar2calendar(vc);
                } else if (type.equals(Constants.TYPE_CALENDAR_SIF) || type.equals(Constants.TYPE_TASK_SIF)) {
                    SIFCalendarParser scp = new SIFCalendarParser(bis);
                    fnblcal = scp.parse();
                }
                if (uid != null) {
                    if (fnblcal.getCalendarContent().getUid() == null) {
                        fnblcal.getCalendarContent().setUid(new Property());
                    }
                    fnblcal.getCalendarContent().getUid().setPropertyValue(uid);
                }
                ical2data = funambol2ical4j.convertFunambolEvent2ical4jEvent(fnblcal, this.connectorProperties, ctx);
            } else{
                CalendarBuilder cbuild = new CalendarBuilder();
                ical2data = cbuild.build(bis);
            } */
            com.funambol.common.pim.calendar.Calendar fnblcal = null;
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            if (type.equals(Constants.TYPE_CALENDAR_VCAL)) {
                XVCalendarParser xvp = new XVCalendarParser(bis);
                VCalendar vc = xvp.XVCalendar();
                VCalendarConverter vcf = new VCalendarConverter(deviceTimeZone, "UTF-8",false);
                fnblcal = vcf.vcalendar2calendar(vc);
                fnblcal.setProdId(new Property("-//bionicmessage.net//Funambol GroupDAV//EN"));
                VCalendar vcal2 = vcf.calendar2vcalendar(fnblcal, false);
                // Remove any empty properties present in Funambol's iCalendar output
                VCalendarContent icc = vcal2.getVCalendarContent();
                List<com.funambol.common.pim.model.Property> props = icc.getAllProperties();
                Iterator<com.funambol.common.pim.model.Property> pIterator = props.iterator();
                while(pIterator.hasNext()) {
                    com.funambol.common.pim.model.Property p = pIterator.next();
                    if (p.getValue() != null && p.getValue().isEmpty()) {
                        pIterator.remove();
                    }
                }
                String icaldata = vcal2.toString();
                CalendarBuilder cbuilder = new CalendarBuilder();
                ByteArrayInputStream vcalBis = new ByteArrayInputStream(icaldata.getBytes());
                ical2data = cbuilder.build(vcalBis);
            } else {
                CalendarBuilder cbuild = new CalendarBuilder();
                ical2data = cbuild.build(bis);
            }
           
            // Make sure the UID is set
            Component firstComponent = getCalendarContent(ical2data);
            setiCalUID(uid);
            // Make sure there is a summary text as well
            net.fortuna.ical4j.model.Property summary =  firstComponent.getProperty(Summary.SUMMARY);
            if (summary == null) {
                summary = new Summary("No summary entered"); // todo: i18n?
                firstComponent.getProperties().add(summary);
            } else if (summary.getValue().length() == 0) {
                summary.setValue("No summary entered");
            }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error while parsing object from " + type, e);
        }
    }


    public String getDefaultStringOutputType() {
        return "text/icalendar";
    }


    public SyncItem getItem() {
        return item;
    }

    public void setItem(SyncItem val) {
        this.item = val;
    }

    public void setDeviceTimeZone(TimeZone tz) {
        deviceTimeZone = tz;
    }

    public TimeZone getDeviceTimeZone() {
        return deviceTimeZone;
    }

    public void setSyncContext(SyncContext ctx) {
        this.ctx = ctx;
    }

    public SyncContext getSyncContext() {
        return ctx;
    }

    public String getUID() {
        Component firstComponent = getCalendarContent(ical2data);
        Uid ui = (Uid) firstComponent.getProperty(Uid.UID);
        return ui.getValue();
    }

    public void setiCalUID(String newUID) {
        Component firstComponent = getCalendarContent(ical2data);
        if (firstComponent.getProperty(Uid.UID) == null) {
            Uid ui = new Uid(uid);
            firstComponent.getProperties().add(ui);
        } else {
            Uid ui = (Uid) firstComponent.getProperty(Uid.UID);
            ui.setValue(newUID);
        }
    }

    public String getSummary() {
        Component firstComponent = getCalendarContent(ical2data);
        Summary su = (Summary) firstComponent.getProperty(Summary.SUMMARY);
        return su.getValue();
    }

    public long getDtStart() {
        Component firstComponent = getCalendarContent(ical2data);
        if (firstComponent.getName().equals(VToDo.VTODO)) {
            return 0;
        }
        DtStart dts = (DtStart) firstComponent.getProperty(DtStart.DTSTART);
        return dts.getDate().getTime();
    }

    public long getDtEnd() {
        Component firstComponent = getCalendarContent(ical2data);
        if (firstComponent.getName().equals(VToDo.VTODO)) {
            return 0;
        }
        DtEnd dte = (DtEnd) firstComponent.getProperty(DtEnd.DTEND);
        return dte.getDate().getTime();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
        setiCalUID(uid);
    }

    public String getEventCategory() {
        Component c = getPIMComponent();
        if (c.getProperty(Categories.CATEGORIES) != null) {
            Categories ca = (Categories) c.getProperty(Categories.CATEGORIES);
            return ca.getValue();
        }
        return null;
    }

    /** Return the first Calendar content component, i.e VEvent or VToDo */
    private CalendarComponent getPIMComponent() {
        CalendarComponent eventComponent = getCalendarContent(ical2data);
        return eventComponent;
    }

    /** Copy attendee data from another calendar (i.e stored server copy)
     * 
     * @param oldCal net.fortuna.ical4j.model.Calendar Calendar object to copy data from
     */
    public void copyAttendeesFromCalendar(Calendar oldCal) {
        if (this.getPIMComponent().getProperties(net.fortuna.ical4j.model.Property.ATTENDEE).size() == 0) {
            Component first = getCalendarContent(oldCal);
            PropertyList attendeeProperties = first.getProperties(net.fortuna.ical4j.model.Property.ATTENDEE);
            for (int i = 0; i < attendeeProperties.size(); i++) {
                Attendee att = (Attendee) attendeeProperties.get(i);
                this.getPIMComponent().getProperties().add(att);
            }
        }
    }

    /** Copy organizer property from another calendar 
     * 
     * @param oldCal net.fortuna.ical4j.model.Calendar Calendar object to copy data from
     */
    public void copyOrganizerFromCalendar(Calendar oldCal) {
        Component first = getCalendarContent(oldCal);
        PropertyList attendeeProperties = first.getProperties(net.fortuna.ical4j.model.Property.ORGANIZER);
        for (int i = 0; i < attendeeProperties.size(); i++) {
            net.fortuna.ical4j.model.Property organizer = (net.fortuna.ical4j.model.Property) attendeeProperties.get(i);
            this.getPIMComponent().getProperties().add(organizer);
        }
    }

    public void copyVAlarmsFromCalendar(Calendar oldCal) {
        //if (this.connectorProperties.getProperty(Constants.CONVERT_ALARMS) == null) {
        ComponentList vAlarms = null;
        CalendarComponent content = getCalendarContent(oldCal);
        // Todo: think of better way to pull alarms without ugly instanceof and casts.
        if (content instanceof net.fortuna.ical4j.model.component.VEvent) {
            vAlarms = ((net.fortuna.ical4j.model.component.VEvent) content).getAlarms();
        } else if (content instanceof net.fortuna.ical4j.model.component.VToDo) {
            vAlarms = ((net.fortuna.ical4j.model.component.VToDo) content).getAlarms();
        }
        for (int i = 0; i < vAlarms.size(); i++) {
            net.fortuna.ical4j.model.component.VAlarm va =
                    (net.fortuna.ical4j.model.component.VAlarm) vAlarms.get(i);
            /* Do NOT carry the VAlarm when its action is to DISPLAY(DALARM) 
             * and we are allowed to convert them */
            if (va.getAction().equals(Action.DISPLAY) && connectorProperties.get(Constants.CONVERT_ALARMS) != null) {
            } else {
                if (getPIMComponent() instanceof net.fortuna.ical4j.model.component.VEvent) {
                    ((net.fortuna.ical4j.model.component.VEvent) getPIMComponent()).getAlarms().add(va);
                } else if (getPIMComponent() instanceof net.fortuna.ical4j.model.component.VToDo) {
                    ((net.fortuna.ical4j.model.component.VToDo) getPIMComponent()).getAlarms().add(va);
                }
            }
        }

    }    //}

    protected CalendarComponent getCalendarContent(Calendar cal) {
        CalendarComponent content =
                (CalendarComponent) cal.getComponents().getComponent(CalendarComponent.VEVENT);
        if (content == null) {
            content = (CalendarComponent) cal.getComponents().getComponent(CalendarComponent.VTODO);
        }
        return content;
    }
}

