/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.calendar;

import com.funambol.common.pim.converter.CalendarToSIFE;
import com.funambol.common.pim.converter.TaskToSIFT;
import com.funambol.common.pim.converter.VCalendarContentConverter;
import com.funambol.common.pim.converter.VCalendarConverter;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.xvcalendar.XVCalendarParser;
import net.bionicmessage.funambol.framework.OutboundObject;
import com.funambol.framework.engine.*;
import com.funambol.framework.engine.source.SyncSource;
import java.io.ByteArrayInputStream;
import net.bionicmessage.funambol.datakit.ical4j2funambol;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.component.CalendarComponent;

/** OutboundFunambolCalendarObject handles the lifecycle of a calendar
 * event/task being sent to a device. 
 * @author Mathew McBride 
 * @see InboundFunambolCalendarObject 
 */
public class OutboundFunambolCalendarObject extends iCalSourceObject implements OutboundObject {

    private net.fortuna.ical4j.model.Calendar ical2 = null;
    private com.funambol.common.pim.calendar.Calendar cal = null;

    /** Default constructor for OutboundFunambolCalendarObject
     * @param ical4jin An ical4j Calendar object to be sent to device
     */
    public OutboundFunambolCalendarObject(Calendar ical4jin) {
        ical2 = ical4jin;
    }

    @Override
    /** Return the original object submitted for transformation */
    public Object getICalObject() {
        return ical2;
    }

    @Override
    /** Replace the object submitted for transformation
     * @param val An net.fortuna.ical4j.model.Calendar object 
     * @exception if val is not an ical4j Calendar
     */
    public void setICalObject(Object val) {
        if (val instanceof net.fortuna.ical4j.model.Calendar)
            ical2 = (net.fortuna.ical4j.model.Calendar)val;
        else
            throw new IllegalArgumentException("val should be an ical4j calendar");
    }


    @Override
    /** Return the original object submitted for transformation
     * Same to getICalObject()
     */
    public Object getPDIDataObject() {
        return ical2;
    }


    @Override
    /** Return the class transformed by this class */
    public Class getPDIDataClass() {
        return net.fortuna.ical4j.model.Calendar.class;
    }


    @Override
    /** Transform the event or task into the desired type and return as a string
     * @param type One of &quot;text/x-vcalendar&quot;, &quot;text/x-s4j-sife&quot;
     *  &quot;text/x-s4j-sift&quot;
     * @return String output of conversion
     * @throw ObjectTransformationException of an exception is encountered during
     *  transform
     * @throw IllegalArgumentException If type is not handled 
     */
    public String getStringRepresentation(String type) throws ObjectTransformationException {
        try {
            if (type.equals(Constants.TYPE_CALENDAR_2445)) {
                return ical2.toString();
            }
            //cal = ical4j2funambol.convertIcal4jToFunambolEvent(ical2, type);
            VCalendarConverter cve = new VCalendarContentConverter(null, "UTF-8", false);
            ByteArrayInputStream bis = new ByteArrayInputStream(ical2.toString().getBytes("UTF-8"));
            XVCalendarParser xvp = new XVCalendarParser(bis);
            VCalendar fnblCalX = xvp.XVCalendar();
            cal = cve.vcalendar2calendar(fnblCalX);
            
            processAlarms();
        if (type.equals(Constants.TYPE_CALENDAR_VCAL)) {
            VCalendar vc = cve.calendar2vcalendar(cal, true);
            cve = null;
            return vc.toString();
        } else if (type.equals(Constants.TYPE_CALENDAR_SIF)) {
            CalendarToSIFE cvx = new CalendarToSIFE(null, "UTF-8");
            return cvx.convert(cal);
        } else if (type.equals(Constants.TYPE_TASK_SIF)) {
            TaskToSIFT ttf = new TaskToSIFT(null, "UTF-8");
            return ttf.convert(cal.getTask());
        }
        throw new IllegalArgumentException("No conversion path for type: "+type);
       } catch (Exception e) {
           throw new ObjectTransformationException("Error getting string representation for "+ type, e);
       }
    }


    @Override
    /** Not implemented for this class */
    public void parseIntoObjectFromString(String str, String type) {
        // Not needed here
    }

    @Override
    /** 
     * Get the default output type for #getStringRepresentation 
     */
    public String getDefaultStringOutputType() {
        return Constants.TYPE_CALENDAR_VCAL;
    }

    /** 
     * Create a Funambol DS SyncItem for this event or task after transforming 
     * @param origin The SyncSource managing this object
     * @return A Funambol SyncItem, transformed to the SyncSource's preferred
     *  data type, or "text/x-vcalendar" if not set
     * @throws java.lang.Exception An exception encounted in the transformation
     *  process, such as ObjectTransformationException
     * @see #getStringRepresentation(String)
     */
    public SyncItem generateSyncItem(SyncSource origin) throws Exception {
        String content = null;
        String type = origin.getInfo().getPreferredType().getType();
        if (type == null) {
            content = getStringRepresentation(Constants.TYPE_CALENDAR_VCAL);
        } else {
            content = getStringRepresentation(type);
        }
        String uid = cal.getCalendarContent().getUid().getPropertyValueAsString();
        SyncItemImpl si = new SyncItemImpl(origin, uid);
        byte[] con = content.getBytes("UTF-8");
        si.setContent(con);
        return si;
    }
    /** 
     * Process alarms embedded in the icalendar data for possible addition 
     * on the device. As this has dangerous ramifications in certain environments,
     * it is disabled by default. When the Constants.CONVERT_ALARMS property is enabled,
     * the connector will process alarms. To customize this functionality, override 
     * this function
     */
    protected void processAlarms() throws ObjectTransformationException {
        if (connectorProperties.getProperty(Constants.CONVERT_ALARMS) != null) {
            ical4j2funambol.attachAlarm(cal.getCalendarContent(), 
                    (CalendarComponent)ical2.getComponents().get(0));
        }
    }
}
