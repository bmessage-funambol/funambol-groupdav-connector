/* 
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
package net.bionicmessage.funambol.groupdav.calendar;

import java.util.Properties;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.bionicmessage.funambol.framework.SourceObject; 

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.11DA249E-F906-C624-377E-85EA23F7B106]
// </editor-fold> 
public class iCalSourceObject implements SourceObject {

    protected Properties connectorProperties; 
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.C24B1B66-690F-D987-1127-E2226CD6621C]
    // </editor-fold> 
    public static final int TASK_DATA_TYPE = 1;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.40CE63E0-906D-9296-CD7A-5E60966C8799]
    // </editor-fold> 
    public static final int CALENDAR_DATA_TYPE = 2;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D179820F-2084-08B4-A45D-705131E00B06]
    // </editor-fold> 
    private int datatype = 0;;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.FA23498B-A4F8-48A8-AECF-87A6E349300B]
    // </editor-fold> 
    private Object iCalObject = null;;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D025BC33-08AD-433A-49CE-D7646721B605]
    // </editor-fold> 
    public iCalSourceObject () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.F17716D8-DF67-6059-80E0-454E3DC979A9]
    // </editor-fold> 
    public int getDatatype () {
        return datatype;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.5413AC8F-7C67-CB34-F308-B221D7DCDECE]
    // </editor-fold> 
    public void setDatatype (int val) {
        this.datatype = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.A9C2B50A-3747-7945-DEE4-28F34CB4F100]
    // </editor-fold> 
    public Object getICalObject () {
        return iCalObject;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.FE7EB492-901D-B756-0527-8F3CF2C34BB8]
    // </editor-fold> 
    public void setICalObject (Object val) {
        this.iCalObject = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D74B81FC-075A-8CF7-F993-F408E6613BCA]
    // </editor-fold> 
    public Object getPDIDataObject () {
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.C9BC716C-9081-FCCE-BF96-2416D89C53CB]
    // </editor-fold> 
    public Class getPDIDataClass () {
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.39B93EF3-9B82-F97F-DE7C-D72CEE615F32]
    // </editor-fold> 
    public String getStringRepresentation (String type) throws ObjectTransformationException {
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2A20D92F-3A45-64F2-C23F-71A4CD831D91]
    // </editor-fold> 
    public void parseIntoObjectFromString (String str, String type) throws ObjectTransformationException {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.C28C7285-590D-326E-C7C2-4811FE8F444E]
    // </editor-fold> 
    public String getDefaultStringOutputType () {
        return null;
    }

    public void setConnectorProperties(Properties properties) {
        this.connectorProperties = properties;
    }

    public Properties getConnectorProperties() {
        return this.connectorProperties;
    }
    
    
}

