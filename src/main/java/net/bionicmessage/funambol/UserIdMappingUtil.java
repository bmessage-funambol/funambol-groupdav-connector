/* GroupDAV connector for Funambol
 * Copyright (C) 2006-2011 Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.funambol.framework.tools.DataSourceTools;
import com.funambol.framework.tools.DBTools;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author matt
 */
public class UserIdMappingUtil {

    protected static final String CORE_DATASOURCE_JNDINAME = "jdbc/fnblcore";

    protected static final String GET_MAP_SQL = "select userid from fnbl_groupdav_user_idmap where fnbluser = ?";
    private static Connection getDatabaseConnection() throws NamingException, SQLException {
        DataSource dataSource = DataSourceTools.lookupDataSource(CORE_DATASOURCE_JNDINAME);
        Connection conn = null;
        conn = dataSource.getConnection();
        return conn;
    }

    public static String getIdForUser(final String userName) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String mappedId = null;
        try {
            conn = getDatabaseConnection();
            ps = conn.prepareStatement(GET_MAP_SQL);
            ps.setString(1, userName);

            rs = ps.executeQuery();
            if (rs.next())
                mappedId = rs.getString("userid");
            
        } finally {
            DBTools.close(conn, ps, rs);
        }
        return mappedId;
    }
}
