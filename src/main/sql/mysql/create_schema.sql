CREATE TABLE IF NOT EXISTS `fnbl_groupdav_user_idmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fnbluser` varchar(255) NOT NULL,
  `userid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);