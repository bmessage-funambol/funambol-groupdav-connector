DROP TABLE fnbl_groupdav_user_idmap IF EXISTS;

CREATE TABLE fnbl_groupdav_user_idmap (
  id int NOT NULL IDENTITY,
  fnbluser varchar(255) NOT NULL,
  userid varchar(255) NOT NULL,
  PRIMARY KEY (id)
);