delete from fnbl_sync_source_type where id='groupdavCal';
insert into fnbl_sync_source_type(id, description, class, admin_class)
values('groupdavCal', 'GroupDAV Calendar', 'net.bionicmessage.funambol.groupdav.calendar.CalendarSyncSource',
'net.bionicmessage.funambol.admin.MultipleCalendarSourceAdminPanel');

delete from fnbl_sync_source_type where id='groupdavContact';
insert into fnbl_sync_source_type(id, description, class, admin_class)
values('groupdavContact', 'GroupDAV Contacts', 'net.bionicmessage.funambol.groupdav.contacts.ContactSyncSource',
'net.bionicmessage.funambol.admin.MultipleContactSourceAdminPanel');

delete from fnbl_sync_source_type where id='groupdavNote';
insert into fnbl_sync_source_type(id, description, class, admin_class)
values('groupdavNote', 'GroupDAV Notes', 'net.bionicmessage.funambol.groupdav.notes.NoteSyncSource',
'net.bionicmessage.funambol.admin.MultipleNoteSourceAdminPanel');

delete from fnbl_module where id='groupdav';
insert into fnbl_module (id, name, description)
values('groupdav','groupdav','GroupDAV');

delete from fnbl_connector where id='groupdav';
insert into fnbl_connector(id, name, description, admin_class)
values('groupdav','BionicMessageGroupDAVConnector','BionicMessage GroupDAV 2.0','');

delete from fnbl_connector_source_type where connector='groupdav' and sourcetype='groupdavCal';
insert into fnbl_connector_source_type(connector, sourcetype)
values('groupdav','groupdavCal');

delete from fnbl_connector_source_type where connector='groupdav' and sourcetype='groupdavContact';
insert into fnbl_connector_source_type(connector, sourcetype)
values('groupdav','groupdavContact');

delete from fnbl_connector_source_type where connector='groupdav' and sourcetype='groupdavNote';
insert into fnbl_connector_source_type(connector, sourcetype)
values('groupdav','groupdavNote');

delete from fnbl_module_connector where module='groupdav' and connector='groupdav';
insert into fnbl_module_connector(module, connector)
values('groupdav','groupdav');
