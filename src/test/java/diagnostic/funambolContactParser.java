/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

package diagnostic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.funambol.common.pim.contact.*;
import com.funambol.common.pim.converter.*;
import com.funambol.common.pim.vcard.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Small sandbox-type test for Funambol contact parser
 * @author matt
 */
public class funambolContactParser {

    public funambolContactParser() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test 
    public void testProblemVCardToVCard() throws Exception {
       // try {
        File testFile = new File ("doc/problem-samples/filled-vcard.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        /* Fix problems with Citadel lowercase begin:vcard etc.. */
        byte[] data = new byte[fis.available()];
        fis.read(data);
        String tofix = new String(data, "UTF-8");
        tofix = tofix.replace("begin:", "BEGIN:").replace("end:", "END:").replace("vcard", "VCARD");
        ByteArrayInputStream bis = new ByteArrayInputStream(tofix.getBytes("UTF-8"));
        VcardParser vcp = new VcardParser(bis, null, "UTF-8");
        Contact vc = vcp.vCard();
        ContactToVcard convert = new ContactToVcard(null, "UTF-8");
        String converted = convert.convert(vc);
        if (!converted.contains("Company INC.")) {
            throw new Exception("Output does not contain organization");
        }
        System.out.println(converted);
        fis.close();
      //  }
      //  catch (Exception e) {
      //  e.printStackTrace();
        
       // }
    }
    
    @Test 
    public void testVCardAttendees() throws Exception {
       // try {
        //File testFile = new File ("doc/testData/vcal1-attendees.vcs");
        File testFile = new File ("/tmp/test.vcf");
        FileInputStream fis = new FileInputStream(testFile);
        /* Fix problems with Citadel lowercase begin:vcard etc.. */
        byte[] data = new byte[fis.available()];
        fis.read(data);
        String tofix = new String(data, "UTF-8");
        tofix = tofix.replace("begin:", "BEGIN:").replace("end:", "END:").replace("vcard", "VCARD");
        ByteArrayInputStream bis = new ByteArrayInputStream(tofix.getBytes("UTF-8"));
        VcardParser vcp = new VcardParser(bis, null, "UTF-8");
        Contact vc = vcp.vCard();
        ContactToVcard convert = new ContactToVcard(null, "UTF-8");
        String converted = convert.convert(vc);
        if (!converted.contains("Company INC.")) {
            throw new Exception("Output does not contain organization");
        }
        System.out.println(converted);
        fis.close();
      //  }
      //  catch (Exception e) {
      //  e.printStackTrace();
        
       // }
    }
}