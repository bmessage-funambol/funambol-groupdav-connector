/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package diagnostic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.funambol.common.pim.calendar.*;
import com.funambol.common.pim.converter.VCalendarConverter;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.xvcalendar.XVCalendarParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.util.List;
import net.fortuna.ical4j.data.CalendarBuilder;

/**
 *
 * @author matt
 */
public class funambolCalParser {

    public funambolCalParser() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test 
    public void testNokiaMemo() throws Exception {
       // try {
        File testFile = new File ("doc/problem-samples/nokia-memo.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        VCalendar blowback = vcf.calendar2vcalendar(cal, true);
        String result = blowback.toString();
        System.out.print(result);
      //  }
      //  catch (Exception e) {
      //  e.printStackTrace();
        
       // }
    }
    
     @Test 
    public void testVcalAttendees() throws Exception {
       // try {
        File testFile = new File ("doc/testData/vcal1-attendees.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        List<Attendee> attendees = cal.getCalendarContent().getAttendees();
        assertTrue("Attendees not in CalendarContent",(attendees.size() > 0));
        for(Attendee at : attendees) {
            System.out.format("Attendee: %s %s\n", at.getName(), at.getEmail()).flush();
        }
        VCalendar blowback = vcf.calendar2vcalendar(cal, true);
        String result = blowback.toString();
        System.out.print(result);
      //  }
      //  catch (Exception e) {
      //  e.printStackTrace();
        
       // }
    }

    @Test
    public void testLocalTimeWithTZ() throws Exception {
        File testFile = new File("doc/testData/localtime.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8",false);
        Calendar cal = vcf.vcalendar2calendar(vc);
        VCalendar ical = vcf.calendar2vcalendar(cal, false);
        String icalOutput = ical.toString();
        assertTrue("Has correct TZNAME in Output", icalOutput.contains("TZNAME:Australia/Melbourne"));
        System.out.println("Funambol iCalendar OUTPUT: ");
        System.out.println(icalOutput);
        System.out.println();
        CalendarBuilder cbuild = new CalendarBuilder();
        net.fortuna.ical4j.model.Calendar cd = cbuild.build(new StringReader(icalOutput));
        System.out.println(cd.toString());
    }

}