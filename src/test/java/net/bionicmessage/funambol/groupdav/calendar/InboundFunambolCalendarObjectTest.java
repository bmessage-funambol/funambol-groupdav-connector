/*
 * InboundFunambolCalendarObjectTest.java
 * Created Feb 24, 2008
 * 
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.calendar;

import com.funambol.framework.engine.InMemorySyncItem;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemImpl;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.security.Sync4jPrincipal;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.bionicmessage.funambol.groupdav.calendar.plugins.InboundDualSourceCategorySwitch;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matt
 */
public class InboundFunambolCalendarObjectTest {

    Sync4jPrincipal spp = null;
    SyncContext ctx = null;
    Calendar cal = null;
    byte[] vcalData;
    byte[] siData;
    byte[] otData;
    public InboundFunambolCalendarObjectTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        spp = Sync4jPrincipal.createPrincipal("", "");
        spp.setUsername("");
        ctx = new SyncContext(spp, 0, null, null, 0);
        File serverFile = new File("doc/testData/korganizer-two-alarms.ics");
        FileInputStream fis = new FileInputStream(serverFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        cal = cbuild.build(fis);
        File vcalFile = new File("doc/testData/korganizer-two-alarms-d.vcs");
        FileInputStream vcalis = new FileInputStream(vcalFile);
        vcalData = new byte[(int) vcalFile.length()];
        int read = 0;
        while ((read = vcalis.read(vcalData, read, vcalData.length - read)) != -1) {
            // Read loop   
        }

        File syncEvolution_test = new File("doc/testData/syncevolution-recur-2nd-tuesday.vcs");
        FileInputStream sis = new FileInputStream(syncEvolution_test);
        siData = new byte[sis.available()];
        sis.read(siData);
        sis.close();

        File outlookTest = new File("doc/testData/outlook-task-empty-properties.vcs");
        FileInputStream ois = new FileInputStream(outlookTest);
        otData = new byte[ois.available()];
        ois.read(otData);
        ois.close();

        vcalis.close();
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    /** Test alarm retention when updating objects */
    public void alarmRetentionTest1() throws Exception {
        SyncItem si = new SyncItemImpl(null, "testkey");
        si.setContent(vcalData);
        si.setType("text/x-vcalendar");
        InboundFunambolCalendarObject ifcb = new InboundFunambolCalendarObject(si);
        Properties fakeConnectorProperties = new Properties();
        ifcb.setConnectorProperties(fakeConnectorProperties);
        ifcb.setSyncContext(ctx);
        ifcb.parseIntoObjectFromString(null, "text/x-vcalendar");
        ifcb.copyVAlarmsFromCalendar(cal);
        String output = ifcb.getStringRepresentation("text/icalendar");
        System.out.println(output);
        assertTrue(output.contains("ACTION:DISPLAY"));
        assertTrue(output.contains("ACTION:AUDIO"));
    }

    @Test
    public void syncEvolutionRecurringEvent() throws ObjectTransformationException {
        InMemorySyncItem si = new InMemorySyncItem(null, "testkey");
        si.setContent(siData);
        si.setType("text/x-vcalendar");
        InboundFunambolCalendarObject ifcb = new InboundFunambolCalendarObject(si);
        ifcb.setConnectorProperties(new Properties());
        ifcb.parseIntoObjectFromString(null, "text/x-vcalendar");
        String output = ifcb.getStringRepresentation("text/calendar");
        System.out.println(output);
    }

    @Test
    public void outlookTaskWithEmptyProperties() throws ObjectTransformationException {
        InMemorySyncItem si = new InMemorySyncItem(null, "testkey");
        si.setContent(otData);
        si.setType("text/x-vcalendar");
        InboundFunambolCalendarObject ifcb = new InboundFunambolCalendarObject(si);
        ifcb.setConnectorProperties(new Properties());
        ifcb.parseIntoObjectFromString(null, "text/x-vcalendar");
        String output = ifcb.getStringRepresentation("text/calendar");
        System.out.println(output);
    }

}
