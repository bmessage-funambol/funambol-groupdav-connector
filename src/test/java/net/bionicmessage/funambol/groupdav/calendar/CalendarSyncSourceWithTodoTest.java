/*
 * CalendarSyncSourceWithTodo.java
 * JUnit 4.x based test
 *
 * Created on Feburary 11, 2008, 6:31 PM
 *
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2008  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.calendar;

import com.funambol.framework.engine.InMemorySyncItem;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemImpl;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSourceException;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.security.Sync4jPrincipal;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import junit.framework.JUnit4TestAdapter;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.groupdav.calendar.CalendarSyncSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author matt
 */
public class CalendarSyncSourceWithTodoTest {

    private String clientId = "STATIC-TEST";
    private String testPropertiesFile = ".groupdavconnector_wtodo_test_properties";
    private CalendarSyncSource css = null;
    private SyncContext ctx = null;
    private Sync4jPrincipal spp = null;
    private Properties testProps = null;

    public CalendarSyncSourceWithTodoTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        // Load a properties file from the local user dir
        String testPropsPath = System.getProperty("user.home") + System.getProperty("file.separator") + testPropertiesFile;
        testProps = new Properties();
        File testPropFile = new File(testPropsPath);
        if (!testPropFile.exists()) {
            System.err.println("CalendarSyncSourceAsTodoTest not configured. Exiting");
            System.err.println("Perhaps you should pass the -Dmaven.test.skip.exec=true flag to build?");
            System.exit(0);
        }
        testProps.load(new FileInputStream(testPropFile));
        css = new CalendarSyncSource();
        String storePath = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "calendarsyncsource-test";
        testProps.setProperty(Constants.STOREDIR_PATH, storePath);
        css.setConnectorProperties(testProps);
        String testUser = testProps.getProperty("test.user");
        spp = Sync4jPrincipal.createPrincipal(testProps.getProperty("test.user"), "testid");
        String passString = String.format("%s:%s", testProps.getProperty("test.user"),
                testProps.getProperty("test.password"));
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        spp.setUsername(testProps.getProperty("test.user"));
        ctx = new SyncContext(spp, 0, null, null, 0);
        SyncSourceInfo ssInfo = new SyncSourceInfo();
        ContentType vcard = new ContentType();
        vcard.setType("text/x-vcalendar");
        vcard.setVersion("1.0");
        ContentType[] supported = new ContentType[1];
        supported[0] = vcard;
        ssInfo.setSupportedTypes(supported);
        ssInfo.setPreferred(0);
        css.setInfo(ssInfo);
    }

    @After
    public void tearDown() throws Exception {
    }

    /** The tests below are in a series of 'easiest' to 'i can't believe its not
     * a human' */
    @Test
    public void beginSlowAndEnd() throws Exception {
        ctx = new SyncContext(spp, 201, null, null, 0);
        css.beginSync(ctx);
        css.endSync();
    }

    @Test
    public void beginSlowGetAllSyncItemKeysEnd() throws Exception {
        ctx = new SyncContext(spp, 201, null, null, 0);
        css.beginSync(ctx);
        SyncItemKey[] all = css.getAllSyncItemKeys();
        for (int i = 0; i < all.length; i++) {
            SyncItemKey syncItemKey = all[i];
            SyncItemKey[] indiv = new SyncItemKey[1];
            indiv[0] = syncItemKey;
            System.out.printf("New/updated Key: %s\n", syncItemKey.getKeyAsString()).flush();
            css.setOperationStatus("Add", 201, indiv);
        }
        css.endSync();
    }

    @Test
    public void beginSlowGetAllSyncItemKeysGetItemsVcalEnd() throws Exception {
        ctx = new SyncContext(spp, 201, null, null, 0);
        css.setType("text/x-vcalendar"); // to fix later
        css.beginSync(ctx);
        SyncItemKey[] all = css.getAllSyncItemKeys();
        for (int i = 0; i < all.length; i++) {
            SyncItemKey syncItemKey = all[i];
            System.out.printf("New/updated Key: %s\n", syncItemKey.getKeyAsString()).flush();
            SyncItem si = css.getSyncItemFromId(syncItemKey);
            String contents = new String(si.getContent());
            System.out.printf("Contents: %s\n------\n", contents).flush();
        }
        css.endSync();
    }

    /** Add two items: one calendar, one todo */
    @Test
    public void beginNormalAddItem() throws Exception {
        ctx = new SyncContext(spp, 200, null, null, 0);
        css.setType("text/x-vcalendar");
        css.beginSync(ctx);
        SyncItem result = loadExample("doc/problem-samples/synthesis-quoted-printable-example.txt",
                "testitem-calsourcetest");
        byte[] newdata = result.getContent();
        String resdata = new String(newdata);
        String resuid = result.getKey().getKeyAsString();
        System.out.printf("Returned from addSyncItem: %s\n, UID: %s\n", resdata, resuid).flush();
        SyncItem todo = loadExample("doc/problem-samples/symbian-todo.vcs",
                "testitem-s60todo");
        byte[] returned_todo = todo.getContent();
        String returned_todo_data = new String(returned_todo, "UTF-8");
        String returned_uid = todo.getKey().getKeyAsString();
        System.out.printf("Returned from addSyncItem for todo: %s\n UID: %s\n", returned_todo_data,
                returned_uid).flush();
        // Write a temp file
        System.setProperty("calsyncsourcetest.beginslowadditem.resuid", resuid);
        System.setProperty("calsyncsourcetest.beginnormaladdtodo.resuid", returned_uid);
        css.endSync();
    }

    @Test
    public void beginNormalDeleteItems() throws Exception {
        ctx = new SyncContext(spp, 200, null, null, 0);
        css.setType("text/x-vcalendar");
        css.beginSync(ctx);
        String uid = System.getProperty("calsyncsourcetest.beginslowadditem.resuid");
        css.removeSyncItem(new SyncItemKey(uid), null, false);
        String todo_uid = System.getProperty("calsyncsourcetest.beginnormaladdtodo.resuid");
        css.removeSyncItem(new SyncItemKey(todo_uid), null, false);
        System.out.printf("Removed item %s from server\n", uid).flush();
        System.out.printf("Removed todo %s from server\n", todo_uid).flush();
        css.endSync();
    }

    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(CalendarSyncSourceWithTodoTest.class);
    }

    private SyncItem loadExample(String examplePath, String uid) throws IOException, FileNotFoundException,
            SyncSourceException {
        File problemSample = new File(examplePath);
        FileInputStream fis = new FileInputStream(problemSample);
        byte[] data = new byte[fis.available()];
        fis.read(data);
        fis.close();
        InMemorySyncItem sync = new InMemorySyncItem(css, uid);
        sync.setContent(data);
        sync.setType("text/x-vcalendar");
        SyncItem result = css.addSyncItem(sync);
        return result;
    }
}
