/*
 * ContactSyncSourceTest.java
 * JUnit 4.x based test
 *
 * Created on October 28, 2007, 2:50 PM
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.bionicmessage.funambol.groupdav.contacts;

import com.funambol.framework.engine.InMemorySyncItem;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.security.Sync4jPrincipal;
import com.funambol.framework.tools.Base64;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import junit.framework.JUnit4TestAdapter;
import net.bionicmessage.funambol.framework.Constants;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matt
 */
public class ContactSyncSourceTwinTest {

    private String clientId = "STATIC-TEST";
    private String testPropertiesFile = ".groupdavconnector_addr_test_properties";
    private ContactSyncSource css = null;
    private SyncContext ctx = null;
    private Sync4jPrincipal spp = null;
    private Properties testProps = null;

    public ContactSyncSourceTwinTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        // Load a properties file from the local user dir
        String testPropsPath = System.getProperty("user.home") + System.getProperty("file.separator") + testPropertiesFile;
        testProps = new Properties();
        testProps.load(new FileInputStream(new File(testPropsPath)));
        css = new ContactSyncSource();
        String storePath = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "contactsyncsource-test";
        String authString = String.format("%s:%s",
                testProps.getProperty("test.user"),
                testProps.getProperty("test.password"));
        byte[] b64auth = Base64.encode(authString.getBytes("UTF-8"));
        testProps.setProperty(Constants.STOREDIR_PATH, storePath);
        css.setConnectorProperties(testProps);
        spp = Sync4jPrincipal.createPrincipal("testuser", "testid");
        spp.setEncodedUserPwd(new String(b64auth));
        ctx = new SyncContext(spp, 0, null, null, 0);
        SyncSourceInfo ssInfo = new SyncSourceInfo();
        ContentType vcard = new ContentType();
        vcard.setType("text/x-vcard");
        vcard.setVersion("2.1");
        ContentType[] supported = new ContentType[1];
        supported[0] = vcard;
        ssInfo.setSupportedTypes(supported);
        ssInfo.setPreferred(0);
        css.setInfo(ssInfo);
    }

    @After
    public void tearDown() throws Exception {
    }

   
    @Test
    public void beginNormalATwinItem() throws Exception {
        ctx = new SyncContext(spp,201,null,null,0);
        css.beginSync(ctx);
        File problemSample = new File("/tmp/4725.vcf");
        FileInputStream fis = new FileInputStream(problemSample);
        byte[] data = new byte[fis.available()];
        fis.read(data);
        fis.close();

        InMemorySyncItem si = new InMemorySyncItem(css, "twintest");
        si.setContent(data);
        si.setType("text/x-vcard");

        SyncItemKey[] twinMatch = css.getSyncItemKeysFromTwin(si);
        css.endSync();

        assertTrue(twinMatch != null);
        assertTrue("Test if there is a twin match", (twinMatch.length > 0));
        String matchUid = "4523";
        
        assertTrue("Test if twin match is right",matchUid.equals(twinMatch[0].getKeyAsString()));
    }
    

    
    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(ContactSyncSourceTwinTest.class);
    } 
}