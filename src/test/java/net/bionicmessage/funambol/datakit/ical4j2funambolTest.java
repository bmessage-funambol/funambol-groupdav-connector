/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bionicmessage.funambol.datakit;

import com.funambol.common.pim.converter.CalendarToSIFE;
import com.funambol.common.pim.converter.VCalendarContentConverter;
import com.funambol.common.pim.converter.VCalendarConverter;
import com.funambol.common.pim.model.VCalendar;
import java.io.File;
import java.io.FileInputStream;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.component.CalendarComponent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matt
 */
public class ical4j2funambolTest {

    public ical4j2funambolTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of convertIcal4jToFunambolEvent method, of class ical4j2funambol.
     */
    @Test
    public void convertIcal4jToFunambolEvent() throws Exception {
        File testFile = new File("doc/problem-samples/ogo-allday.ics");
        FileInputStream fis = new FileInputStream(testFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar cal = cbuild.build(fis);
        com.funambol.common.pim.calendar.Calendar fnblcal =
                ical4j2funambol.convertIcal4jToFunambolEvent(cal, "text/x-vcalendar");
        VCalendarConverter vcc = new VCalendarConverter(null, "UTF-8");
        VCalendar vc = vcc.calendar2vcalendar(fnblcal, true);
        String text = vc.toString();
        System.out.println(text);

    }

    /** Test attaching alarm 
     *
     */
    @Test
    public void testAlarmDurationM15() throws Exception {
        File testFile = new File("doc/testData/korganizer-15min-alarm.ics");
        FileInputStream fis = new FileInputStream(testFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar cal = cbuild.build(fis);
        com.funambol.common.pim.calendar.Calendar fnblcal =
                ical4j2funambol.convertIcal4jToFunambolEvent(cal, "text/x-vcalendar");
        ical4j2funambol.attachAlarm(fnblcal.getCalendarContent(), 
                (CalendarComponent)cal.getComponents().get(0));
        VCalendarConverter vcc = new VCalendarConverter(null, "UTF-8");
        VCalendar vc = vcc.calendar2vcalendar(fnblcal, false);
        String text = vc.toString();
        System.out.println(text);
    }

    /**
     * Test of attachAllDayDtEnd method, of class ical4j2funambol.
     */
    @Test
    public void attachAllDayDtEnd() {
    }

    /**
     * Test of attachAllDayDtStart method, of class ical4j2funambol.
     */
    @Test
    public void attachAllDayDtStart() {
    }
    @Test
    public void testAttendees() throws Exception {
        File testFile = new File("doc/testData/attendees.ics");
        FileInputStream fis = new FileInputStream(testFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar cal = cbuild.build(fis);
        com.funambol.common.pim.calendar.Calendar fnblcal =
                ical4j2funambol.convertIcal4jToFunambolEvent(cal, "text/x-vcalendar");
        ical4j2funambol.attachAlarm(fnblcal.getCalendarContent(), 
                (CalendarComponent)cal.getComponents().get(0));
        // No attendees under vcal. yet
        CalendarToSIFE cale = new CalendarToSIFE(null, "UTF-8");
        String text = (String)cale.convert(fnblcal);
        System.out.println(text);
    }
    @Test
    public void testRecurrence() throws Exception {
        File testFile = new File("doc/testData/recur.ics");
        FileInputStream fis = new FileInputStream(testFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar cal = cbuild.build(fis);
        com.funambol.common.pim.calendar.Calendar fnblcal =
                ical4j2funambol.convertIcal4jToFunambolEvent(cal, "text/x-vcalendar");
        ical4j2funambol.attachAlarm(fnblcal.getCalendarContent(), 
                (CalendarComponent)cal.getComponents().get(0));
        // No attendees under vcal. yet
        CalendarToSIFE cale = new CalendarToSIFE(null, "UTF-8");
        String text = (String)cale.convert(fnblcal);
        System.out.println(text);
        
        VCalendarConverter vcc = new VCalendarContentConverter(null, "UTF-8", false);
        VCalendar vc = vcc.calendar2vcalendar(fnblcal, false);
        String vcal = vc.toString();
        System.out.println(vcal);
    }
    @Test
    public void testQPDecode() throws Exception {
    File testFile = new File("doc/testData/qp.ics");
        FileInputStream fis = new FileInputStream(testFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar cal = cbuild.build(fis);
        com.funambol.common.pim.calendar.Calendar fnblcal =
                ical4j2funambol.convertIcal4jToFunambolEvent(cal, "text/x-vcalendar");
        
        VCalendarConverter vcc = new VCalendarContentConverter(null, "UTF-8", false);
        VCalendar vc = vcc.calendar2vcalendar(fnblcal, true);
        String vcal = vc.toString(); 
        System.out.println(vcal);
        assertTrue(vcal.contains("Test f=C3"));
    }
    @Test
    public void testAllDay() throws Exception {
    File testFile = new File("doc/testData/allday.ics");
        FileInputStream fis = new FileInputStream(testFile);
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar cal = cbuild.build(fis);
        com.funambol.common.pim.calendar.Calendar fnblcal =
                ical4j2funambol.convertIcal4jToFunambolEvent(cal, "text/x-vcalendar");

        VCalendarConverter vcc = new VCalendarContentConverter(null, "UTF-8", false);
        VCalendar vc = vcc.calendar2vcalendar(fnblcal, true);
        String vcal = vc.toString();
        System.out.println(vcal);
    }
    
}
