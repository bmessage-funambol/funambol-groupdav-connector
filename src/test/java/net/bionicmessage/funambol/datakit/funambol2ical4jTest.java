/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.datakit;

import com.funambol.common.pim.model.Property;
import java.util.TimeZone;
import com.funambol.common.pim.calendar.RecurrencePattern;
import com.funambol.common.pim.calendar.Calendar;
import com.funambol.common.pim.converter.VCalendarConverter;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.sif.SIFCalendarParser;
import com.funambol.common.pim.xvcalendar.XVCalendarParser;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.security.Sync4jPrincipal;
import com.funambol.framework.server.Sync4jDevice;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.SimpleTimeZone;
import org.apache.commons.codec.binary.Base64;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matt
 */
public class funambol2ical4jTest {

    public funambol2ical4jTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNokiaMemo() throws Exception {
        // try {
        File testFile = new File("doc/problem-samples/nokia-memo.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal = 
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        System.out.print(output);
    }

    @Test
    public void testAttendee() throws Exception {
        // try {
        File testFile = new File("doc/testData/vcal-attendee-partstat.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal =
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);

        String output = ical4jcal.toString();
        System.out.println("--- ATTENDEE TEST: ");
        System.out.print(output);
    }

    @Test
    public void testCategoryParse() throws Exception {
        // try {
        File testFile = new File("doc/problem-samples/category.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal = 
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        System.out.print(output);
    }
    @Test
    public void testRecur() throws Exception {
        // try {
        File testFile = new File("doc/testData/outlook-recur-vcal.txt");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal = 
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        assertTrue(output.contains("COUNT=10"));
        System.out.print(output);
    }
    @Test
    public void testRecur2() throws Exception {
        // try {
        File testFile = new File("doc/testData/outlook-recur-twodaysaweek.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal =
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        System.out.println("\r\nTEST RECUR 2");
        System.out.println(output);
        System.out.println();
        assertTrue(output.contains("BYDAY=TU,TH"));
    }
    @Test
    /** Prototype testcase used to test modifications needed to parse description
     * text from Synthesis correctly
     */
    public void testX() throws Exception {
        // try {
        File testFile = new File("doc/problem-samples/complicated-comment.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        byte[] data = new byte[(int)testFile.length()];
        fis.read(data);
        String dataString = new String(data, "UTF-8");
        dataString = dataString.replace("=0D=0A=\r\n\r\nDTSTART", "\r\nDTSTART");
        String debugString = dataString.replace("\r","xR").replace("\n","xN");
        byte[] newData = dataString.getBytes("UTF-8");
        ByteArrayInputStream bis = new ByteArrayInputStream(newData);
        XVCalendarParser vcp = new XVCalendarParser(bis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal = 
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        System.out.print(output);
    }

    @Test
    public void testSIFAllDay() throws Exception {
        // try {
        File testFile = new File("doc/problem-samples/SIFAllDay.xml");
        FileInputStream fis = new FileInputStream(testFile);
        SIFCalendarParser scp = new SIFCalendarParser(fis);
        Calendar cal = scp.parse();
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal = 
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        System.out.print(output);
    }
    @Test
    public void testAndroidAllDay() throws Exception {
        // try {
        File testFile = new File("doc/problem-samples/android-all-day.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        VCalendarConverter vcf = new VCalendarConverter(null, "UTF-8");
        Calendar cal = vcf.vcalendar2calendar(vc);
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal =
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        System.out.print(output);
    }

    @Test
    public void testTimeZoneRecur() throws Exception {
        // try {
        File testFile = new File("doc/testData/android-recur-weekly-to.vcs");
        FileInputStream fis = new FileInputStream(testFile);
        XVCalendarParser vcp = new XVCalendarParser(fis);
        VCalendar vc = vcp.XVCalendar();
        TimeZone tz = TimeZone.getTimeZone("Australia/Melbourne");
        VCalendarConverter vcf = new VCalendarConverter(tz, "UTF-8",false);
        Calendar cal = vcf.vcalendar2calendar(vc);
        RecurrencePattern rp = cal.getCalendarContent().getRecurrencePattern();
        assertTrue("Correct number of days in list", (rp.getDayOfWeek().size() == 1));
        Properties fakeSyncSourceProps = new Properties();
        Sync4jPrincipal spp = Sync4jPrincipal.createPrincipal("user", "testid");
        String passString = "user:pass";
        Sync4jDevice s4d = new Sync4jDevice("fakedevice");
        s4d.setTimeZone("Australia/Melbourne");
        spp.setDevice(s4d);
        String encodedPassString = new String(Base64.encodeBase64(passString.getBytes()));
        spp.setEncodedUserPwd(encodedPassString);
        SyncContext ctx = new SyncContext(spp, 0, null, null, 0);
        net.fortuna.ical4j.model.Calendar ical4jcal =
                funambol2ical4j.convertFunambolEvent2ical4jEvent(cal, fakeSyncSourceProps, ctx);
        String output = ical4jcal.toString();
        assertTrue(output.contains("RRULE:FREQ=WEEKLY;UNTIL=20110530T000000Z\r\n"));
    }
}